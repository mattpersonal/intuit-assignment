//
//  RepositoriesServiceTests.swift
//  Intuit-AssignmentTests
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import Intuit_Assignment

class RepositoriesServiceTests: XCTestCase {
    
    private var repositoriesService: RepositoriesService!

    override func setUp() {
        repositoriesService = RepositoriesService()
    }

    func test_fetchRepositories() {
        let testExpecation = expectation(description: "test expectation")
        
        _ = repositoriesService.fetchRepositories(from: "www.example.com",
                                                  networkingStrategy: .mock(data: MockData.success, statusCode: 200),
                                                  completion: { result in
                                                    switch result {
                                                    case let .success(repositories):
                                                        XCTAssert(repositories == MockData.expectedSuccessResponse)
                                                        
                                                    case let .failure(error):
                                                        XCTFail("Unexpected failure with error: \(error.localizedDescription)")
                                                    }
                                                    
                                                    testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
}
