//
//  IssuesServiceTests.swift
//  Intuit-AssignmentTests
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import Intuit_Assignment

class IssuesServiceTests: XCTestCase {
    
    private var issuesService: IssuesService!
    
    override func setUp() {
        issuesService = IssuesService()
    }
    
    func test_getIssues() {
        let testExpecation = expectation(description: "test expectation")
        
        _ = issuesService.fetchIssues(from: "www.example.com",
                                      networkingStrategy: .mock(data: MockData.success, statusCode: 200),
                                      completion: { result in
                                        switch result {
                                        case let .success(issues):
                                            XCTAssert(issues == MockData.expectedSuccessResponse)
                                            
                                        case let .failure(error):
                                            XCTFail("Unexpected failure with error: \(error.localizedDescription)")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }

}
