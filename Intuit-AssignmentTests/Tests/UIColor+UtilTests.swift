//
//  UIColor+UtilTests.swift
//  Intuit-AssignmentTests
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import Intuit_Assignment

class UIColorUtilTests: XCTestCase {
    
    func test_initHexString_black() {
        let hexString = "000000"
        
        let color = UIColor(hexString: hexString)
        
        XCTAssertNotNil(color , "UIColor initializer unexpectedly failed.")
        XCTAssert(areColorsEqualInRGB(color1: color, color2: .black), "Expected .black color")
    }
    
    func test_initHexString_white() {
        let hexString = "FFFFFF"
        
        let color = UIColor(hexString: hexString)
        
        XCTAssertNotNil(color , "UIColor initializer unexpectedly failed.")
        XCTAssert(areColorsEqualInRGB(color1: color, color2: .white), "Expected .white color")
    }
    
    func test_initHexString_InvalidLength() {
        let hexString = "FFFFFFFFF"
        
        let color = UIColor(hexString: hexString)
        
        XCTAssertNil(color , "UIColor initializer unexpectedly succeeded.")
    }
    
    func test_initHexString_InvalidCharacters() {
        let hexString = "These characters are not valid"
        
        let color = UIColor(hexString: hexString)
        
        XCTAssertNil(color , "UIColor initializer unexpectedly succeeded.")
    }
    
    func test_initHexString_PoundSignPrefix() {
        let hexString = "#000000"
        
        let color = UIColor(hexString: hexString)
        
        XCTAssertNotNil(color , "UIColor initializer unexpectedly failed.")
        XCTAssert(areColorsEqualInRGB(color1: color, color2: .black), "Expected .black color")
    }
    
    func test_rgb() {
        let rgbValues = UIColor.white.rgb()
        
        XCTAssertNotNil(rgbValues, "RGB unexpectedly failed")
        XCTAssert(rgbValues?.red == 255)
        XCTAssert(rgbValues?.green == 255)
        XCTAssert(rgbValues?.blue == 255)
        XCTAssert(rgbValues?.alpha == 1)
    }
    
    private func areColorsEqualInRGB(color1: UIColor?, color2: UIColor?) -> Bool {
        guard let color1 = color1, let color2 = color2 else {
            return false
        }
        
        guard let rgb1 = color1.rgb(), let rgb2 = color2.rgb() else {
            return false
        }
        
        return rgb1 == rgb2
    }
}
