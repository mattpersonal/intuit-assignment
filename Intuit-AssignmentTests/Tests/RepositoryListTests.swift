//
//  RepositoryListTests.swift
//  Intuit-AssignmentTests
//
//  Created by Matthew Richardson on 11/4/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import Intuit_Assignment

class RepositoryListTests: XCTestCase {

    private var mockService: MockRepositoriesService!
    private var viewModel: RepositoryListViewModel!
    private var mockView: MockRepositoryListView!
    
    override func setUp() {
        // start with fresh view model and view each time.
        mockService = MockRepositoriesService()
        viewModel = RepositoryListViewModel(repositoriesService: mockService)
        mockView = MockRepositoryListView(viewModel: viewModel)
    }
    
    // Testing the initial state once the view model is created
    func test_viewModel_init() {
        // view model has already been initialized - just verify the state
        XCTAssert(mockView.screenTitle == .app(.repoListTitle))
        XCTAssert(mockView.sectionViewModels.count == 0)
        XCTAssert(mockView.viewState == .loading)
        XCTAssert(mockView.shownRepository == nil)
        XCTAssert(mockView.showRepositoryDetailsCalledCount == 0)
    }
    
    // Testing the state when the view model receives an error while loading the data
    func test_viewModel_loadData_error() {
        let completionExpectation = expectation(description: "waiting for async tasks to complete")
        
        // use failing status code
        mockService.mockStatusCode = 400
        mockService.mockData = MockData.success
        
        viewModel.loadData {
            
            // We have to wait for a timer since the communication between the ViewModel and the View uses Combine (CurrenValueSubject, PassthroughSubject, Publisher, etc). The subscribers in the View may receive the event after the completion handler is called. A simple delay allows for the publishers to send events. I recognize that Timers are not ideal in Unit Tests since the delay time is seemingly arbitrary and the delay may only be enough for some processors. However, Combine is usually very efficient and I'm not concerned about too many false positives with this design.
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                XCTAssert(self.mockView.screenTitle == .app(.repoListTitle))
                XCTAssert(self.mockView.sectionViewModels.count == 0)
                XCTAssert(self.mockView.shownRepository == nil)
                XCTAssert(self.mockView.showRepositoryDetailsCalledCount == 0)
                
                switch self.mockView.viewState {
                case let .error(errorString):
                    // success - we expected error.
                    XCTAssert(errorString == .app(.repoListErrorLoadingData))
                    
                default:
                    XCTFail("Expected error state.")
                }
                
                completionExpectation.fulfill()
            }
        }
        
        let result = XCTWaiter().wait(for: [completionExpectation], timeout: 5)
        XCTAssert(result == .completed, "operation did not complete")
    }
    
    func test_viewModel_loadData_success() {
        let completionExpectation = expectation(description: "waiting for async tasks to complete")
        
        // use data to produce success
        mockService.mockStatusCode = 200
        mockService.mockData = MockData.success
        
        viewModel.loadData {
            
            // We have to wait for a timer since the communication between the ViewModel and the View uses Combine (CurrenValueSubject, PassthroughSubject, Publisher, etc). The subscribers in the View may receive the event after the completion handler is called. A simple delay allows for the publishers to send events. I recognize that Timers are not ideal in Unit Tests since the delay time is seemingly arbitrary and the delay may only be enough for some processors. However, Combine is usually very efficient and I'm not concerned about too many false positives with this design.
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                XCTAssert(self.mockView.screenTitle == .app(.repoListTitle))
                XCTAssert(self.mockView.sectionViewModels.count == 2)
                XCTAssert(self.mockView.shownRepository == nil)
                XCTAssert(self.mockView.showRepositoryDetailsCalledCount == 0)
                
                switch self.mockView.viewState {
                case .displayingData:
                    break // expected
                    
                default:
                    XCTFail("Expected displayingData state.")
                }
                
                // verify the first section view model
                let firstSectionViewModel = self.mockView.sectionViewModels[0]
                let mockFirstSectionView = MockRepositoryListSectionView(viewModel: firstSectionViewModel)
                XCTAssert(mockFirstSectionView.shouldDisplayHeader == true)
                XCTAssert(mockFirstSectionView.repositoryOwnerViewModel.ownerName == Expected.Success.FirstRepo.ownerLogin)
                XCTAssert(mockFirstSectionView.displayedRepositoryViewModels.count == 1)

                let firstRepoItemViewModel = mockFirstSectionView.displayedRepositoryViewModels[0]
                let mockFirstItemView = MockRepositoryListItemView(viewModel: firstRepoItemViewModel)
                XCTAssert(mockFirstItemView.isRepositoryPrivate == Expected.Success.FirstRepo.isRepositoryPrivate)
                XCTAssert(mockFirstItemView.repositoryAccessLevelName == Expected.Success.FirstRepo.accessLevelName)
                XCTAssert(mockFirstItemView.repositoryAccessImage == Expected.Success.FirstRepo.accessLevelImage)
                XCTAssert(mockFirstItemView.repositoryName == Expected.Success.FirstRepo.name)
                XCTAssert(mockFirstItemView.repositoryDescription == Expected.Success.FirstRepo.description)

                // verify the second section view model
                let secondSectionViewModel = self.mockView.sectionViewModels[1]
                let mockSecondSectionView = MockRepositoryListSectionView(viewModel: secondSectionViewModel)
                XCTAssert(mockSecondSectionView.shouldDisplayHeader == true)
                XCTAssert(mockSecondSectionView.repositoryOwnerViewModel.ownerName == Expected.Success.SecondRepo.ownerLogin)
                XCTAssert(mockSecondSectionView.displayedRepositoryViewModels.count == 1)
                
                let secondRepoItemViewModel = mockSecondSectionView.displayedRepositoryViewModels[0]
                let mockSecondItemView = MockRepositoryListItemView(viewModel: secondRepoItemViewModel)
                XCTAssert(mockSecondItemView.isRepositoryPrivate == Expected.Success.SecondRepo.isRepositoryPrivate)
                XCTAssert(mockSecondItemView.repositoryAccessLevelName == Expected.Success.SecondRepo.accessLevelName)
                XCTAssert(mockSecondItemView.repositoryAccessImage == Expected.Success.SecondRepo.accessLevelImage)
                XCTAssert(mockSecondItemView.repositoryName == Expected.Success.SecondRepo.name)
                XCTAssert(mockSecondItemView.repositoryDescription == Expected.Success.SecondRepo.description)
                
                completionExpectation.fulfill()
            }
        }
        
        let result = XCTWaiter().wait(for: [completionExpectation], timeout: 5)
        XCTAssert(result == .completed, "operation did not complete")
    }
    
    // Testing when use searchs and the search string matches none of the repositories
    func test_viewModel_userUpdatedSearch_noResults() {
        let completionExpectation = expectation(description: "waiting for async tasks to complete")
        
        // setup for success
        mockService.mockStatusCode = 200
        mockService.mockData = MockData.success
        
        viewModel.loadData {
            
            // We have to wait for a timer since the communication between the ViewModel and the View uses Combine (CurrenValueSubject, PassthroughSubject, Publisher, etc). The subscribers in the View may receive the event after the completion handler is called. A simple delay allows for the publishers to send events. I recognize that Timers are not ideal in Unit Tests since the delay time is seemingly arbitrary and the delay may only be enough for some processors. However, Combine is usually very efficient and I'm not concerned about too many false positives with this design.
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                self.viewModel.userUpdatedSearch(Expected.searchStringMatchingNeitherRepo)
                
                // again - wait for updates to be propagated to views
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                    
                    // search has completed and view has been updated - check data
                    XCTAssert(self.mockView.screenTitle == .app(.repoListTitle))
                    XCTAssert(self.mockView.sectionViewModels.count == 2)
                    XCTAssert(self.mockView.shownRepository == nil)
                    XCTAssert(self.mockView.showRepositoryDetailsCalledCount == 0)
                    
                    switch self.mockView.viewState {
                    case let .emptyData(displayString):
                        XCTAssert(displayString == .app(.repoListEmptyData))
                        
                    default:
                        XCTFail("Expected displayingData state.")
                    }
                    
                    // Since no data is displaying, the section view models should not be displaying any data
                    
                    // verify the first section view model
                    let firstSectionViewModel = self.mockView.sectionViewModels[0]
                    let mockFirstSectionView = MockRepositoryListSectionView(viewModel: firstSectionViewModel)
                    XCTAssert(mockFirstSectionView.shouldDisplayHeader == false)
                    XCTAssert(mockFirstSectionView.displayedRepositoryViewModels.count == 0)
                    
                    // verify the second section view model
                    let secondSectionViewModel = self.mockView.sectionViewModels[1]
                    let mockSecondSectionView = MockRepositoryListSectionView(viewModel: secondSectionViewModel)
                    XCTAssert(mockSecondSectionView.shouldDisplayHeader == false)
                    XCTAssert(mockSecondSectionView.displayedRepositoryViewModels.count == 0)
                    
                    completionExpectation.fulfill()
                }
            }
        }
        
        let result = XCTWaiter().wait(for: [completionExpectation], timeout: 5)
        XCTAssert(result == .completed, "operation did not complete")
    }
    
    // Testing when user searches and the search string matches some but not all of the repositories
    func test_viewModel_userUpdatedSearch_partialResult() {
        let completionExpectation = expectation(description: "waiting for async tasks to complete")
        
        // setup for success
        mockService.mockStatusCode = 200
        mockService.mockData = MockData.success
        
        viewModel.loadData {
            
            // We have to wait for a timer since the communication between the ViewModel and the View uses Combine (CurrenValueSubject, PassthroughSubject, Publisher, etc). The subscribers in the View may receive the event after the completion handler is called. A simple delay allows for the publishers to send events. I recognize that Timers are not ideal in Unit Tests since the delay time is seemingly arbitrary and the delay may only be enough for some processors. However, Combine is usually very efficient and I'm not concerned about too many false positives with this design.
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                self.viewModel.userUpdatedSearch(Expected.searchStringMatchingFirstRepo)
                
                // again - wait for updates to be propagated to views
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                    
                    // search has completed and view has been updated - check data
                    XCTAssert(self.mockView.screenTitle == .app(.repoListTitle))
                    XCTAssert(self.mockView.sectionViewModels.count == 2)
                    XCTAssert(self.mockView.shownRepository == nil)
                    XCTAssert(self.mockView.showRepositoryDetailsCalledCount == 0)
                    
                    switch self.mockView.viewState {
                    case .displayingData:
                        // expected
                        break
                        
                    default:
                        XCTFail("Expected displayingData state.")
                    }
                    
                    // verify the first section view model (Should be displaying)
                    let firstSectionViewModel = self.mockView.sectionViewModels[0]
                    let mockFirstSectionView = MockRepositoryListSectionView(viewModel: firstSectionViewModel)
                    XCTAssert(mockFirstSectionView.shouldDisplayHeader == true)
                    XCTAssert(mockFirstSectionView.displayedRepositoryViewModels.count == 1)
                    
                    // verify the second section view model (Should be filtered by the search)
                    let secondSectionViewModel = self.mockView.sectionViewModels[1]
                    let mockSecondSectionView = MockRepositoryListSectionView(viewModel: secondSectionViewModel)
                    XCTAssert(mockSecondSectionView.shouldDisplayHeader == false)
                    XCTAssert(mockSecondSectionView.displayedRepositoryViewModels.count == 0)
                    
                    completionExpectation.fulfill()
                }
            }
        }
        
        let result = XCTWaiter().wait(for: [completionExpectation], timeout: 5)
        XCTAssert(result == .completed, "operation did not complete")
    }
    
    // Testing when user searches and the search string matches all repositories
    func test_viewModel_userUpdatedSearch_allResults() {
        let completionExpectation = expectation(description: "waiting for async tasks to complete")
        
        // setup for success
        mockService.mockStatusCode = 200
        mockService.mockData = MockData.success
        
        viewModel.loadData {
            
            // We have to wait for a timer since the communication between the ViewModel and the View uses Combine (CurrenValueSubject, PassthroughSubject, Publisher, etc). The subscribers in the View may receive the event after the completion handler is called. A simple delay allows for the publishers to send events. I recognize that Timers are not ideal in Unit Tests since the delay time is seemingly arbitrary and the delay may only be enough for some processors. However, Combine is usually very efficient and I'm not concerned about too many false positives with this design.
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                self.viewModel.userUpdatedSearch(Expected.searchStringMatchingBothRepos)
                
                // again - wait for updates to be propagated to views
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                    
                    // search has completed and view has been updated - check data
                    XCTAssert(self.mockView.screenTitle == .app(.repoListTitle))
                    XCTAssert(self.mockView.sectionViewModels.count == 2)
                    XCTAssert(self.mockView.shownRepository == nil)
                    XCTAssert(self.mockView.showRepositoryDetailsCalledCount == 0)
                    
                    switch self.mockView.viewState {
                    case .displayingData:
                        // expected
                        break
                        
                    default:
                        XCTFail("Expected displayingData state.")
                    }
                    
                    // verify the first section view model (Should be displaying)
                    let firstSectionViewModel = self.mockView.sectionViewModels[0]
                    let mockFirstSectionView = MockRepositoryListSectionView(viewModel: firstSectionViewModel)
                    XCTAssert(mockFirstSectionView.shouldDisplayHeader == true)
                    XCTAssert(mockFirstSectionView.displayedRepositoryViewModels.count == 1)
                    
                    // verify the second section view model (Should be displaying)
                    let secondSectionViewModel = self.mockView.sectionViewModels[1]
                    let mockSecondSectionView = MockRepositoryListSectionView(viewModel: secondSectionViewModel)
                    XCTAssert(mockSecondSectionView.shouldDisplayHeader == true)
                    XCTAssert(mockSecondSectionView.displayedRepositoryViewModels.count == 1)
                    
                    completionExpectation.fulfill()
                }
            }
        }
        
        let result = XCTWaiter().wait(for: [completionExpectation], timeout: 5)
        XCTAssert(result == .completed, "operation did not complete")
    }
    
    func test_viewModel_userTappedRepositoryItem() {
        let completionExpectation = expectation(description: "waiting for async tasks to complete")
        
        // setup for success
        mockService.mockStatusCode = 200
        mockService.mockData = MockData.success
        
        viewModel.loadData {
            
            // We have to wait for a timer since the communication between the ViewModel and the View uses Combine (CurrenValueSubject, PassthroughSubject, Publisher, etc). The subscribers in the View may receive the event after the completion handler is called. A simple delay allows for the publishers to send events. I recognize that Timers are not ideal in Unit Tests since the delay time is seemingly arbitrary and the delay may only be enough for some processors. However, Combine is usually very efficient and I'm not concerned about too many false positives with this design.
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                
                // tap on one the first item
                let firstSectionViewModel = self.mockView.sectionViewModels[0]
                let mockFirstSectionView = MockRepositoryListSectionView(viewModel: firstSectionViewModel)
                let firstItemViewModel = mockFirstSectionView.displayedRepositoryViewModels[0]
                firstItemViewModel.userDidTapView()
                
                // again - wait for updates to be propagated to views
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { _ in
                    
                    // verify that showRepositoryDetails was propagated to the main list view
                    XCTAssert(self.mockView.screenTitle == .app(.repoListTitle))
                    XCTAssert(self.mockView.sectionViewModels.count == 2)
                    XCTAssert(self.mockView.shownRepository == Expected.Success.firstRepository)
                    XCTAssert(self.mockView.showRepositoryDetailsCalledCount == 1)
                    
                    completionExpectation.fulfill()
                }
            }
        }
        
        let result = XCTWaiter().wait(for: [completionExpectation], timeout: 5)
        XCTAssert(result == .completed, "operation did not complete")
    }
}
