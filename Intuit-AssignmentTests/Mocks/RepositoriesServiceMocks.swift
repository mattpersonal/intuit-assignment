//
//  RepositoriesServiceMocks.swift
//  Intuit-AssignmentTests
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

@testable import Intuit_Assignment
import Foundation

extension RepositoriesServiceTests {
    
    enum MockData {
        static let success: Data = encode(json: MockData.successJson)
        
        static let expectedSuccessResponse: [Repository] = {
            let owner = GithubUser(login: "intuit",
                                   id: 2495066,
                                   avatarUrlString: "https://avatars2.githubusercontent.com/u/2495066?v=4")
            
            let license = Repository.License(name: "MIT License",
                                            licenseUrlString: "https://api.github.com/licenses/mit")
            
            let repo = Repository(id: 41687006,
                                  name: "ami-query",
                                  fullName: "intuit/ami-query",
                                  owner: owner,
                                  description: "Provide a REST interface to your organizations AMIs",
                                  issuesUrlString: "https://api.github.com/repos/intuit/ami-query/issues{/number}",
                                  isRepositoryPrivate: false,
                                  openIssuesCount: 1,
                                  starCount: 28,
                                  watcherCount: 28,
                                  forkCount: 10,
                                  license: license,
                                  contributorsUrlString: "https://api.github.com/repos/intuit/ami-query/contributors")
            
            return [repo]
        }()
        
        private static let successJson = """
            [
              {
                "id": 41687006,
                "node_id": "MDEwOlJlcG9zaXRvcnk0MTY4NzAwNg==",
                "name": "ami-query",
                "full_name": "intuit/ami-query",
                "private": false,
                "owner": {
                  "login": "intuit",
                  "id": 2495066,
                  "node_id": "MDEyOk9yZ2FuaXphdGlvbjI0OTUwNjY=",
                  "avatar_url": "https://avatars2.githubusercontent.com/u/2495066?v=4",
                  "gravatar_id": "",
                  "url": "https://api.github.com/users/intuit",
                  "html_url": "https://github.com/intuit",
                  "followers_url": "https://api.github.com/users/intuit/followers",
                  "following_url": "https://api.github.com/users/intuit/following{/other_user}",
                  "gists_url": "https://api.github.com/users/intuit/gists{/gist_id}",
                  "starred_url": "https://api.github.com/users/intuit/starred{/owner}{/repo}",
                  "subscriptions_url": "https://api.github.com/users/intuit/subscriptions",
                  "organizations_url": "https://api.github.com/users/intuit/orgs",
                  "repos_url": "https://api.github.com/users/intuit/repos",
                  "events_url": "https://api.github.com/users/intuit/events{/privacy}",
                  "received_events_url": "https://api.github.com/users/intuit/received_events",
                  "type": "Organization",
                  "site_admin": false
                },
                "html_url": "https://github.com/intuit/ami-query",
                "description": "Provide a REST interface to your organizations AMIs",
                "fork": false,
                "url": "https://api.github.com/repos/intuit/ami-query",
                "forks_url": "https://api.github.com/repos/intuit/ami-query/forks",
                "keys_url": "https://api.github.com/repos/intuit/ami-query/keys{/key_id}",
                "collaborators_url": "https://api.github.com/repos/intuit/ami-query/collaborators{/collaborator}",
                "teams_url": "https://api.github.com/repos/intuit/ami-query/teams",
                "hooks_url": "https://api.github.com/repos/intuit/ami-query/hooks",
                "issue_events_url": "https://api.github.com/repos/intuit/ami-query/issues/events{/number}",
                "events_url": "https://api.github.com/repos/intuit/ami-query/events",
                "assignees_url": "https://api.github.com/repos/intuit/ami-query/assignees{/user}",
                "branches_url": "https://api.github.com/repos/intuit/ami-query/branches{/branch}",
                "tags_url": "https://api.github.com/repos/intuit/ami-query/tags",
                "blobs_url": "https://api.github.com/repos/intuit/ami-query/git/blobs{/sha}",
                "git_tags_url": "https://api.github.com/repos/intuit/ami-query/git/tags{/sha}",
                "git_refs_url": "https://api.github.com/repos/intuit/ami-query/git/refs{/sha}",
                "trees_url": "https://api.github.com/repos/intuit/ami-query/git/trees{/sha}",
                "statuses_url": "https://api.github.com/repos/intuit/ami-query/statuses/{sha}",
                "languages_url": "https://api.github.com/repos/intuit/ami-query/languages",
                "stargazers_url": "https://api.github.com/repos/intuit/ami-query/stargazers",
                "contributors_url": "https://api.github.com/repos/intuit/ami-query/contributors",
                "subscribers_url": "https://api.github.com/repos/intuit/ami-query/subscribers",
                "subscription_url": "https://api.github.com/repos/intuit/ami-query/subscription",
                "commits_url": "https://api.github.com/repos/intuit/ami-query/commits{/sha}",
                "git_commits_url": "https://api.github.com/repos/intuit/ami-query/git/commits{/sha}",
                "comments_url": "https://api.github.com/repos/intuit/ami-query/comments{/number}",
                "issue_comment_url": "https://api.github.com/repos/intuit/ami-query/issues/comments{/number}",
                "contents_url": "https://api.github.com/repos/intuit/ami-query/contents/{+path}",
                "compare_url": "https://api.github.com/repos/intuit/ami-query/compare/{base}...{head}",
                "merges_url": "https://api.github.com/repos/intuit/ami-query/merges",
                "archive_url": "https://api.github.com/repos/intuit/ami-query/{archive_format}{/ref}",
                "downloads_url": "https://api.github.com/repos/intuit/ami-query/downloads",
                "issues_url": "https://api.github.com/repos/intuit/ami-query/issues{/number}",
                "pulls_url": "https://api.github.com/repos/intuit/ami-query/pulls{/number}",
                "milestones_url": "https://api.github.com/repos/intuit/ami-query/milestones{/number}",
                "notifications_url": "https://api.github.com/repos/intuit/ami-query/notifications{?since,all,participating}",
                "labels_url": "https://api.github.com/repos/intuit/ami-query/labels{/name}",
                "releases_url": "https://api.github.com/repos/intuit/ami-query/releases{/id}",
                "deployments_url": "https://api.github.com/repos/intuit/ami-query/deployments",
                "created_at": "2015-08-31T16:23:44Z",
                "updated_at": "2019-10-29T15:03:30Z",
                "pushed_at": "2019-10-30T20:24:29Z",
                "git_url": "git://github.com/intuit/ami-query.git",
                "ssh_url": "git@github.com:intuit/ami-query.git",
                "clone_url": "https://github.com/intuit/ami-query.git",
                "svn_url": "https://github.com/intuit/ami-query",
                "homepage": null,
                "size": 1287,
                "stargazers_count": 28,
                "watchers_count": 28,
                "language": "Go",
                "has_issues": true,
                "has_projects": true,
                "has_downloads": true,
                "has_wiki": true,
                "has_pages": false,
                "forks_count": 10,
                "mirror_url": null,
                "archived": false,
                "disabled": false,
                "open_issues_count": 1,
                "license": {
                  "key": "mit",
                  "name": "MIT License",
                  "spdx_id": "MIT",
                  "url": "https://api.github.com/licenses/mit",
                  "node_id": "MDc6TGljZW5zZTEz"
                },
                "forks": 10,
                "open_issues": 1,
                "watchers": 28,
                "default_branch": "master"
              }
            ]
        """
        
        private static func encode(json: String) -> Data {
            return json.data(using: .utf8)!
        }
    }
}
