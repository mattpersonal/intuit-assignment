//
//  RepositoryListMocks.swift
//  Intuit-AssignmentTests
//
//  Created by Matthew Richardson on 11/4/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import RealTalk
import ReactiveExtras
@testable import Intuit_Assignment

extension RepositoryListTests {
    
    class MockRepositoriesService: RepositoriesServiceApi {
        
        var mockData: Data = Data(capacity: 0)
        var mockStatusCode: Int = 200
        
        init() {}
        
        func fetchRepositories(from repositoriesUrlString: String?,
                               networkingStrategy: Networker.NetworkingStrategy,
                               completion: @escaping (Result<[Repository], Networker.NetworkError>) -> Void) -> URLSessionDataTask? {
            
            // ignore the networkingStrategy and always mock the data
            let request = RepositoriesNetworkRequest(repositoriesUrlString: "www.example.com")
            
            return Networker.shared.perform(request: request,
                                            networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                            completionQueue: .main,
                                            completion: completion)
        }
    }
    
    class MockRepositoryListView {
        
        // Properties exposed for testing
        let screenTitle: String?
        var sectionViewModels: [RepositoryListSectionInput]
        var viewState: RepositoryListViewState
        var showRepositoryDetailsCalledCount = 0 // Keeps track of the number of times the details showRepositoryDetailsPublisher handler was called
        var shownRepository: Repository? // the last repo to be shown
        
        private let viewModel: RepositoryListViewInput
        private var cancelBag = CancellableBag()

        init(viewModel: RepositoryListViewInput) {
            self.viewModel = viewModel
            
            screenTitle = viewModel.screenTitle
            sectionViewModels = viewModel.sectionViewModels.value
            viewState = viewModel.viewState.value

            cancelBag << viewModel.sectionViewModels.receive(on: DispatchQueue.main).sink(receiveValue: { [weak self] models in
                self?.sectionViewModels = models
            })
            
            cancelBag << viewModel.viewState.receive(on: DispatchQueue.main).sink(receiveValue: { [weak self] state in
                self?.viewState = state
            })
            
            cancelBag << viewModel.showRepositoryDetailsPublisher.receive(on: DispatchQueue.main).sink(receiveValue: { [weak self] repository in
                self?.shownRepository = repository
                self?.showRepositoryDetailsCalledCount += 1
            })
        }
    }
    
    class MockRepositoryListSectionView {
        
        // Properties exposed for testing
        var shouldDisplayHeader: Bool
        var repositoryOwnerViewModel: RepositoryListOwnerViewModel
        var displayedRepositoryViewModels: [RepositoryItemViewModel]
        
        private let viewModel: RepositoryListSectionInput
        private var cancelBag = CancellableBag()
        
        init(viewModel: RepositoryListSectionInput) {
            self.viewModel = viewModel
            
            shouldDisplayHeader = viewModel.shouldDisplayHeader
            repositoryOwnerViewModel = viewModel.repositoryOwnerViewModel
            displayedRepositoryViewModels = viewModel.displayedRepositoryViewModels.elementsReadyOnly
            
            cancelBag << viewModel.displayedRepositoryViewModels.receive(on: DispatchQueue.main).sink(receiveValue: { [weak self] arrayOperation in
                switch arrayOperation {
                case let .update(array):
                    self?.displayedRepositoryViewModels = array

                case let .insert(array, _):
                    self?.displayedRepositoryViewModels = array

                case let .remove(array, _):
                    self?.displayedRepositoryViewModels = array

                case let .replace(array, _, _, _):
                    self?.displayedRepositoryViewModels = array

                case let .move(array, _, _):
                    self?.displayedRepositoryViewModels = array
                }
            })
        }
    }
    
    class MockRepositoryListItemView {
        
        // Properties exposed for testing
        var repositoryName: String?
        var repositoryDescription: String?
        var repositoryAccessLevelName: String?
        var repositoryAccessImage: UIImage?
        var isRepositoryPrivate: Bool
        
        private let viewModel: RepositoryItemViewInput
        
        init(viewModel: RepositoryItemViewInput) {
            self.viewModel = viewModel
            
            repositoryName = viewModel.repositoryName
            repositoryDescription = viewModel.repositoryDescription
            repositoryAccessImage = viewModel.repositoryAccessImage
            repositoryAccessLevelName = viewModel.repositoryAccessLevelName
            isRepositoryPrivate = viewModel.isRepositoryPrivate
        }
    }
    
    enum Expected {
        
        static let searchStringMatchingFirstRepo = "abc-first"
        static let searchStringMatchingBothRepos = "abc"
        static let searchStringMatchingNeitherRepo = "no matches for me"
        
        enum Success {
            enum FirstRepo {
                static let id: Int? = 1
                static let name: String? = "abc-firstRepoName"
                static let fullName: String? = "\(FirstRepo.ownerLogin!)/\(FirstRepo.name!)"
                static let ownerLogin: String? = "login1"
                static let ownerId: Int? = 1
                static let ownerAvatarUrlString: String? = "www.avatar1.com"
                static let description: String? = "description1"
                static let issuesUrlString: String? = "www.issues1.com"
                static let isRepositoryPrivate = true
                static let openIssuesCount: Int? = 1
                static let startCount: Int? = 1
                static let watcherCount: Int? = 1
                static let forkCount: Int? = 1
                static let licenseName: String? = "license1"
                static let licenseUrlString: String? = "www.license1.com"
                static let contributorsUrlString: String? = "www.contributors1.com"
                static let accessLevelName: String? = .app(.repoListPrivateAccess)
                static let accessLevelImage: UIImage? = .app(.lock)
            }
            
            static let firstRepository: Repository = {
                let owner = GithubUser(login: FirstRepo.ownerLogin,
                                       id: FirstRepo.ownerId,
                                       avatarUrlString: FirstRepo.ownerAvatarUrlString)
                
                let license = Repository.License(name: FirstRepo.licenseName,
                                                 licenseUrlString: FirstRepo.licenseUrlString)
                
                return Repository(id: FirstRepo.id,
                                  name: FirstRepo.name,
                                  fullName: FirstRepo.fullName,
                                  owner: owner,
                                  description: FirstRepo.description,
                                  issuesUrlString: FirstRepo.issuesUrlString,
                                  isRepositoryPrivate: FirstRepo.isRepositoryPrivate,
                                  openIssuesCount: FirstRepo.openIssuesCount,
                                  starCount: FirstRepo.startCount,
                                  watcherCount: FirstRepo.watcherCount,
                                  forkCount: FirstRepo.forkCount,
                                  license: license,
                                  contributorsUrlString: FirstRepo.contributorsUrlString)
            }()
            
            enum SecondRepo {
                static let id = 2
                static let name: String? = "abc-secondRepoName"
                static let fullName: String? = "\(SecondRepo.ownerLogin!)/\(SecondRepo.name!)"
                static let ownerLogin: String? = "login2"
                static let ownerId: Int? = 2
                static let ownerAvatarUrlString: String? = "www.avatar2.com"
                static let description: String? = "description2"
                static let issuesUrlString: String? = "www.issues2.com"
                static let isRepositoryPrivate = false
                static let openIssuesCount: Int? = 2
                static let startCount: Int? = 2
                static let watcherCount: Int? = 2
                static let forkCount: Int? = 2
                static let licenseName: String? = "license2"
                static let licenseUrlString: String? = "www.license2.com"
                static let contributorsUrlString: String? = "www.contributors2.com"
                static let accessLevelName: String? = .app(.repoListPublicAccess)
                static let accessLevelImage: UIImage? = .app(.lockOpen)
            }
        }
    }
    
    enum MockData {
        
        static let success = encode(json: MockData.successJson)
        
        private static let successJson = """
            [
              {
                "id": \(Expected.Success.FirstRepo.id!),
                "node_id": "MDEwOlJlcG9zaXRvcnk0MTY4NzAwNg==",
                "name": "\(Expected.Success.FirstRepo.name!)",
                "full_name": "\(Expected.Success.FirstRepo.fullName!)",
                "private": \(Expected.Success.FirstRepo.isRepositoryPrivate),
                "owner": {
                      "login": "\(Expected.Success.FirstRepo.ownerLogin!)",
                      "id": \(Expected.Success.FirstRepo.ownerId!),
                      "node_id": "MDEyOk9yZ2FuaXphdGlvbjI0OTUwNjY=",
                      "avatar_url": "\(Expected.Success.FirstRepo.ownerAvatarUrlString!)",
                      "gravatar_id": "",
                      "url": "https://api.github.com/users/intuit",
                      "html_url": "https://github.com/intuit",
                      "followers_url": "https://api.github.com/users/intuit/followers",
                      "following_url": "https://api.github.com/users/intuit/following{/other_user}",
                      "gists_url": "https://api.github.com/users/intuit/gists{/gist_id}",
                      "starred_url": "https://api.github.com/users/intuit/starred{/owner}{/repo}",
                      "subscriptions_url": "https://api.github.com/users/intuit/subscriptions",
                      "organizations_url": "https://api.github.com/users/intuit/orgs",
                      "repos_url": "https://api.github.com/users/intuit/repos",
                      "events_url": "https://api.github.com/users/intuit/events{/privacy}",
                      "received_events_url": "https://api.github.com/users/intuit/received_events",
                      "type": "Organization",
                      "site_admin": false
                },
                "html_url": "https://github.com/intuit/ami-query",
                "description": "\(Expected.Success.FirstRepo.description!)",
                "fork": false,
                "url": "https://api.github.com/repos/intuit/ami-query",
                "forks_url": "https://api.github.com/repos/intuit/ami-query/forks",
                "keys_url": "https://api.github.com/repos/intuit/ami-query/keys{/key_id}",
                "collaborators_url": "https://api.github.com/repos/intuit/ami-query/collaborators{/collaborator}",
                "teams_url": "https://api.github.com/repos/intuit/ami-query/teams",
                "hooks_url": "https://api.github.com/repos/intuit/ami-query/hooks",
                "issue_events_url": "https://api.github.com/repos/intuit/ami-query/issues/events{/number}",
                "events_url": "https://api.github.com/repos/intuit/ami-query/events",
                "assignees_url": "https://api.github.com/repos/intuit/ami-query/assignees{/user}",
                "branches_url": "https://api.github.com/repos/intuit/ami-query/branches{/branch}",
                "tags_url": "https://api.github.com/repos/intuit/ami-query/tags",
                "blobs_url": "https://api.github.com/repos/intuit/ami-query/git/blobs{/sha}",
                "git_tags_url": "https://api.github.com/repos/intuit/ami-query/git/tags{/sha}",
                "git_refs_url": "https://api.github.com/repos/intuit/ami-query/git/refs{/sha}",
                "trees_url": "https://api.github.com/repos/intuit/ami-query/git/trees{/sha}",
                "statuses_url": "https://api.github.com/repos/intuit/ami-query/statuses/{sha}",
                "languages_url": "https://api.github.com/repos/intuit/ami-query/languages",
                "stargazers_url": "https://api.github.com/repos/intuit/ami-query/stargazers",
                "contributors_url": "\(Expected.Success.FirstRepo.contributorsUrlString!)",
                "subscribers_url": "https://api.github.com/repos/intuit/ami-query/subscribers",
                "subscription_url": "https://api.github.com/repos/intuit/ami-query/subscription",
                "commits_url": "https://api.github.com/repos/intuit/ami-query/commits{/sha}",
                "git_commits_url": "https://api.github.com/repos/intuit/ami-query/git/commits{/sha}",
                "comments_url": "https://api.github.com/repos/intuit/ami-query/comments{/number}",
                "issue_comment_url": "https://api.github.com/repos/intuit/ami-query/issues/comments{/number}",
                "contents_url": "https://api.github.com/repos/intuit/ami-query/contents/{+path}",
                "compare_url": "https://api.github.com/repos/intuit/ami-query/compare/{base}...{head}",
                "merges_url": "https://api.github.com/repos/intuit/ami-query/merges",
                "archive_url": "https://api.github.com/repos/intuit/ami-query/{archive_format}{/ref}",
                "downloads_url": "https://api.github.com/repos/intuit/ami-query/downloads",
                "issues_url": "\(Expected.Success.FirstRepo.issuesUrlString!)",
                "pulls_url": "https://api.github.com/repos/intuit/ami-query/pulls{/number}",
                "milestones_url": "https://api.github.com/repos/intuit/ami-query/milestones{/number}",
                "notifications_url": "https://api.github.com/repos/intuit/ami-query/notifications{?since,all,participating}",
                "labels_url": "https://api.github.com/repos/intuit/ami-query/labels{/name}",
                "releases_url": "https://api.github.com/repos/intuit/ami-query/releases{/id}",
                "deployments_url": "https://api.github.com/repos/intuit/ami-query/deployments",
                "created_at": "2015-08-31T16:23:44Z",
                "updated_at": "2019-10-29T15:03:30Z",
                "pushed_at": "2019-10-30T20:24:29Z",
                "git_url": "git://github.com/intuit/ami-query.git",
                "ssh_url": "git@github.com:intuit/ami-query.git",
                "clone_url": "https://github.com/intuit/ami-query.git",
                "svn_url": "https://github.com/intuit/ami-query",
                "homepage": null,
                "size": 1287,
                "stargazers_count": \(Expected.Success.FirstRepo.startCount!),
                "watchers_count": \(Expected.Success.FirstRepo.watcherCount!),
                "language": "Go",
                "has_issues": true,
                "has_projects": true,
                "has_downloads": true,
                "has_wiki": true,
                "has_pages": false,
                "forks_count": \(Expected.Success.FirstRepo.forkCount!),
                "mirror_url": null,
                "archived": false,
                "disabled": false,
                "open_issues_count": \(Expected.Success.FirstRepo.openIssuesCount!),
                "license": {
                  "key": "mit",
                  "name": "\(Expected.Success.FirstRepo.licenseName!)",
                  "spdx_id": "MIT",
                  "url": "\(Expected.Success.FirstRepo.licenseUrlString!)",
                  "node_id": "MDc6TGljZW5zZTEz"
                },
                "forks": \(Expected.Success.FirstRepo.forkCount!),
                "open_issues": \(Expected.Success.FirstRepo.openIssuesCount!),
                "watchers": \(Expected.Success.FirstRepo.watcherCount!),
                "default_branch": "master"
              },
              {
                "id": \(Expected.Success.SecondRepo.id),
                "node_id": "MDEwOlJlcG9zaXRvcnk0MTY4NzAwNg==",
                "name": "\(Expected.Success.SecondRepo.name!)",
                "full_name": "\(Expected.Success.SecondRepo.fullName!)",
                "private": \(Expected.Success.SecondRepo.isRepositoryPrivate),
                "owner": {
                    "login": "\(Expected.Success.SecondRepo.ownerLogin!)",
                    "id": \(Expected.Success.SecondRepo.ownerId!),
                    "node_id": "MDEyOk9yZ2FuaXphdGlvbjI0OTUwNjY=",
                    "avatar_url": "\(Expected.Success.SecondRepo.ownerAvatarUrlString!)",
                    "gravatar_id": "",
                    "url": "https://api.github.com/users/intuit",
                    "html_url": "https://github.com/intuit",
                    "followers_url": "https://api.github.com/users/intuit/followers",
                    "following_url": "https://api.github.com/users/intuit/following{/other_user}",
                    "gists_url": "https://api.github.com/users/intuit/gists{/gist_id}",
                    "starred_url": "https://api.github.com/users/intuit/starred{/owner}{/repo}",
                    "subscriptions_url": "https://api.github.com/users/intuit/subscriptions",
                    "organizations_url": "https://api.github.com/users/intuit/orgs",
                    "repos_url": "https://api.github.com/users/intuit/repos",
                    "events_url": "https://api.github.com/users/intuit/events{/privacy}",
                    "received_events_url": "https://api.github.com/users/intuit/received_events",
                    "type": "Organization",
                    "site_admin": false
                },
                "html_url": "https://github.com/intuit/ami-query",
                "description": "\(Expected.Success.SecondRepo.description!)",
                "fork": false,
                "url": "https://api.github.com/repos/intuit/ami-query",
                "forks_url": "https://api.github.com/repos/intuit/ami-query/forks",
                "keys_url": "https://api.github.com/repos/intuit/ami-query/keys{/key_id}",
                "collaborators_url": "https://api.github.com/repos/intuit/ami-query/collaborators{/collaborator}",
                "teams_url": "https://api.github.com/repos/intuit/ami-query/teams",
                "hooks_url": "https://api.github.com/repos/intuit/ami-query/hooks",
                "issue_events_url": "https://api.github.com/repos/intuit/ami-query/issues/events{/number}",
                "events_url": "https://api.github.com/repos/intuit/ami-query/events",
                "assignees_url": "https://api.github.com/repos/intuit/ami-query/assignees{/user}",
                "branches_url": "https://api.github.com/repos/intuit/ami-query/branches{/branch}",
                "tags_url": "https://api.github.com/repos/intuit/ami-query/tags",
                "blobs_url": "https://api.github.com/repos/intuit/ami-query/git/blobs{/sha}",
                "git_tags_url": "https://api.github.com/repos/intuit/ami-query/git/tags{/sha}",
                "git_refs_url": "https://api.github.com/repos/intuit/ami-query/git/refs{/sha}",
                "trees_url": "https://api.github.com/repos/intuit/ami-query/git/trees{/sha}",
                "statuses_url": "https://api.github.com/repos/intuit/ami-query/statuses/{sha}",
                "languages_url": "https://api.github.com/repos/intuit/ami-query/languages",
                "stargazers_url": "https://api.github.com/repos/intuit/ami-query/stargazers",
                "contributors_url": "\(Expected.Success.SecondRepo.contributorsUrlString!)",
                "subscribers_url": "https://api.github.com/repos/intuit/ami-query/subscribers",
                "subscription_url": "https://api.github.com/repos/intuit/ami-query/subscription",
                "commits_url": "https://api.github.com/repos/intuit/ami-query/commits{/sha}",
                "git_commits_url": "https://api.github.com/repos/intuit/ami-query/git/commits{/sha}",
                "comments_url": "https://api.github.com/repos/intuit/ami-query/comments{/number}",
                "issue_comment_url": "https://api.github.com/repos/intuit/ami-query/issues/comments{/number}",
                "contents_url": "https://api.github.com/repos/intuit/ami-query/contents/{+path}",
                "compare_url": "https://api.github.com/repos/intuit/ami-query/compare/{base}...{head}",
                "merges_url": "https://api.github.com/repos/intuit/ami-query/merges",
                "archive_url": "https://api.github.com/repos/intuit/ami-query/{archive_format}{/ref}",
                "downloads_url": "https://api.github.com/repos/intuit/ami-query/downloads",
                "issues_url": "\(Expected.Success.SecondRepo.issuesUrlString!)",
                "pulls_url": "https://api.github.com/repos/intuit/ami-query/pulls{/number}",
                "milestones_url": "https://api.github.com/repos/intuit/ami-query/milestones{/number}",
                "notifications_url": "https://api.github.com/repos/intuit/ami-query/notifications{?since,all,participating}",
                "labels_url": "https://api.github.com/repos/intuit/ami-query/labels{/name}",
                "releases_url": "https://api.github.com/repos/intuit/ami-query/releases{/id}",
                "deployments_url": "https://api.github.com/repos/intuit/ami-query/deployments",
                "created_at": "2015-08-31T16:23:44Z",
                "updated_at": "2019-10-29T15:03:30Z",
                "pushed_at": "2019-10-30T20:24:29Z",
                "git_url": "git://github.com/intuit/ami-query.git",
                "ssh_url": "git@github.com:intuit/ami-query.git",
                "clone_url": "https://github.com/intuit/ami-query.git",
                "svn_url": "https://github.com/intuit/ami-query",
                "homepage": null,
                "size": 1287,
                "stargazers_count": \(Expected.Success.SecondRepo.startCount!),
                "watchers_count": \(Expected.Success.SecondRepo.watcherCount!),
                "language": "Go",
                "has_issues": true,
                "has_projects": true,
                "has_downloads": true,
                "has_wiki": true,
                "has_pages": false,
                "forks_count": \(Expected.Success.SecondRepo.forkCount!),
                "mirror_url": null,
                "archived": false,
                "disabled": false,
                "open_issues_count": \(Expected.Success.SecondRepo.openIssuesCount!),
                "license": {
                    "key": "mit",
                    "name": "\(Expected.Success.SecondRepo.licenseName!)",
                    "spdx_id": "MIT",
                    "url": "\(Expected.Success.SecondRepo.licenseUrlString!)",
                    "node_id": "MDc6TGljZW5zZTEz"
                },
                "forks": \(Expected.Success.SecondRepo.forkCount!),
                "open_issues": \(Expected.Success.SecondRepo.openIssuesCount!),
                "watchers": \(Expected.Success.SecondRepo.watcherCount!),
                "default_branch": "master"
              }
            ]
        """
        
        private static func encode(json: String) -> Data {
            return json.data(using: .utf8)!
        }
    }
}
