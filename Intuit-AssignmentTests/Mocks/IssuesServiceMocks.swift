//
//  IssuesServiceMocks.swift
//  Intuit-AssignmentTests
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
@testable import Intuit_Assignment

extension IssuesServiceTests {
    
    enum MockData {
        static let success: Data = encode(json: MockData.successJson)
        
        static let expectedSuccessResponse: [Repository.Issue] = {
            let state = Repository.Issue.State.open(name: "open")
            
            let user = GithubUser(login: "pandian912",
                                  id: 1931157,
                                  avatarUrlString: "https://avatars1.githubusercontent.com/u/1931157?v=4")
            
            let label = Repository.Issue.Label(id: 1383884013,
                                              name: "bug",
                                              color: UIColor(hexString: "d73a4a"))
            
            var components = DateComponents()
            components.timeZone = TimeZone(secondsFromGMT: 0)
            components.year = 2019
            components.month = 10
            components.day = 21
            components.hour = 23
            components.minute = 27
            components.second = 32
            let expectedDate = Calendar.current.date(from: components)
            
            let issue = Repository.Issue(number: 2,
                                         title: "Blocking port script fails",
                                         body: "Is this your feature request related to a problem?",
                                         state: state,
                                         dateCreated: expectedDate,
                                         dateClosed: expectedDate,
                                         user: user,
                                         labels: [label],
                                         assignees: [user],
                                         commentCount: 8,
                                         commentsUrlString: "https://api.github.com/repos/intuit/CloudRaider/issues/2/comments")
            
            return [issue]
        }()
        
        private static let successJson = """
            [
              {
                "url": "https://api.github.com/repos/intuit/CloudRaider/issues/2",
                "repository_url": "https://api.github.com/repos/intuit/CloudRaider",
                "labels_url": "https://api.github.com/repos/intuit/CloudRaider/issues/2/labels{/name}",
                "comments_url": "https://api.github.com/repos/intuit/CloudRaider/issues/2/comments",
                "events_url": "https://api.github.com/repos/intuit/CloudRaider/issues/2/events",
                "html_url": "https://github.com/intuit/CloudRaider/issues/2",
                "id": 501031047,
                "node_id": "MDU6SXNzdWU1MDEwMzEwNDc=",
                "number": 2,
                "title": "Blocking port script fails",
                "body": "Is this your feature request related to a problem?",
                "created_at": "2019-10-21T23:27:32Z",
                "closed_at": "2019-10-21T23:27:32Z",
                "user": {
                  "login": "pandian912",
                  "id": 1931157,
                  "node_id": "MDQ6VXNlcjE5MzExNTc=",
                  "avatar_url": "https://avatars1.githubusercontent.com/u/1931157?v=4",
                  "gravatar_id": "",
                  "url": "https://api.github.com/users/pandian912",
                  "html_url": "https://github.com/pandian912",
                  "followers_url": "https://api.github.com/users/pandian912/followers",
                  "following_url": "https://api.github.com/users/pandian912/following{/other_user}",
                  "gists_url": "https://api.github.com/users/pandian912/gists{/gist_id}",
                  "starred_url": "https://api.github.com/users/pandian912/starred{/owner}{/repo}",
                  "subscriptions_url": "https://api.github.com/users/pandian912/subscriptions",
                  "organizations_url": "https://api.github.com/users/pandian912/orgs",
                  "repos_url": "https://api.github.com/users/pandian912/repos",
                  "events_url": "https://api.github.com/users/pandian912/events{/privacy}",
                  "received_events_url": "https://api.github.com/users/pandian912/received_events",
                  "type": "User",
                  "site_admin": false
                },
                "labels": [
                  {
                    "id": 1383884013,
                    "node_id": "MDU6TGFiZWwxMzgzODg0MDEz",
                    "url": "https://api.github.com/repos/intuit/CloudRaider/labels/bug",
                    "name": "bug",
                    "color": "d73a4a",
                    "default": true
                  }
                ],
                "state": "open",
                "comments": 8,
                "locked": false,
                "assignee": {
                  "login": "pandian912",
                  "id": 1931157,
                  "node_id": "MDQ6VXNlcjE5MzExNTc=",
                  "avatar_url": "https://avatars1.githubusercontent.com/u/1931157?v=4",
                  "gravatar_id": "",
                  "url": "https://api.github.com/users/pandian912",
                  "html_url": "https://github.com/pandian912",
                  "followers_url": "https://api.github.com/users/pandian912/followers",
                  "following_url": "https://api.github.com/users/pandian912/following{/other_user}",
                  "gists_url": "https://api.github.com/users/pandian912/gists{/gist_id}",
                  "starred_url": "https://api.github.com/users/pandian912/starred{/owner}{/repo}",
                  "subscriptions_url": "https://api.github.com/users/pandian912/subscriptions",
                  "organizations_url": "https://api.github.com/users/pandian912/orgs",
                  "repos_url": "https://api.github.com/users/pandian912/repos",
                  "events_url": "https://api.github.com/users/pandian912/events{/privacy}",
                  "received_events_url": "https://api.github.com/users/pandian912/received_events",
                  "type": "User",
                  "site_admin": false
                },
                "assignees": [
                  {
                    "login": "pandian912",
                    "id": 1931157,
                    "node_id": "MDQ6VXNlcjE5MzExNTc=",
                    "avatar_url": "https://avatars1.githubusercontent.com/u/1931157?v=4",
                    "gravatar_id": "",
                    "url": "https://api.github.com/users/pandian912",
                    "html_url": "https://github.com/pandian912",
                    "followers_url": "https://api.github.com/users/pandian912/followers",
                    "following_url": "https://api.github.com/users/pandian912/following{/other_user}",
                    "gists_url": "https://api.github.com/users/pandian912/gists{/gist_id}",
                    "starred_url": "https://api.github.com/users/pandian912/starred{/owner}{/repo}",
                    "subscriptions_url": "https://api.github.com/users/pandian912/subscriptions",
                    "organizations_url": "https://api.github.com/users/pandian912/orgs",
                    "repos_url": "https://api.github.com/users/pandian912/repos",
                    "events_url": "https://api.github.com/users/pandian912/events{/privacy}",
                    "received_events_url": "https://api.github.com/users/pandian912/received_events",
                    "type": "User",
                    "site_admin": false
                  }
                ]
              }
            ]
        """
    }
    
    private static func encode(json: String) -> Data {
        return json.data(using: .utf8)!
    }
}
