//
//  Repository.License.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Repository {
    
    struct License: Decodable, Equatable {
        let name: String?
        let licenseUrlString: String?
        
        enum CodingKeys: String, CodingKey {
            case name
            case licenseUrlString = "url"
        }
    }
}
