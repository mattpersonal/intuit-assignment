//
//  Repository.Issue.Comment.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Repository.Issue {
    
    struct Comment: Decodable, Equatable {
        let user: GithubUser?
        let dateCreated: Date?
        let body: String?
        
        enum CodingKeys: String, CodingKey {
            case user
            case dateCreated = "created_at"
            case body
        }
    }
}
