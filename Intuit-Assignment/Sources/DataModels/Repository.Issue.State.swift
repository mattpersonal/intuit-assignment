//
//  Repository.Issue.State.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Repository.Issue {
    
    /// Various states for a repository issue
    enum State: Equatable {
        
        // NOTE: I would model others (I assume "closed" would be one), but I'm not seeing other states. Maybe I could root through Github specs to find a list of states, but I'm only seeing "open" in the examples. So I'm going to roll with it.
        case open(name: String?)
        case other(name: String?)
    }
}
