//
//  Repository.Issue.Label.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension Repository.Issue {
    
    /// A lable/tag for an issue (like bug, active, etc.)
    struct Label: Decodable, Equatable {
        let id: Int?
        let name: String?
        let color: UIColor?
        
        init(id: Int?,
             name: String?,
             color: UIColor?) {
            self.id = id
            self.name = name
            self.color = color
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            id = try container.decodeIfPresent(Int.self, forKey: .id)
            name = try container.decodeIfPresent(String.self, forKey: .name)
            
            if let colorString = try container.decodeIfPresent(String.self, forKey: .color) {
                color = UIColor(hexString: colorString)
            } else {
                color = nil
            }
        }
        
        enum CodingKeys: String, CodingKey {
            case id
            case name
            case color
        }
    }
}
