//
//  Repository.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

struct Repository: Decodable, Equatable {
    let id: Int?
    let name: String?
    let fullName: String?
    let owner: GithubUser?
    let description: String?
    let issuesUrlString: String?
    let isRepositoryPrivate: Bool?
    let openIssuesCount: Int?
    let starCount: Int?
    let watcherCount: Int?
    let forkCount: Int?
    let license: Repository.License?
    let contributorsUrlString: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case owner
        case description
        case issuesUrlString = "issues_url"
        case isRepositoryPrivate = "private"
        case openIssuesCount = "open_issues_count"
        case starCount = "stargazers_count"
        case watcherCount = "watchers_count"
        case forkCount = "forks"
        case license
        case contributorsUrlString = "contributors_url"
    }
}
