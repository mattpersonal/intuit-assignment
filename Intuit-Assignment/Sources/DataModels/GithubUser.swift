//
//  GithubUser.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

struct GithubUser: Decodable, Equatable, Comparable, Hashable {
    let login: String?
    let id: Int?
    let avatarUrlString: String?
    
    enum CodingKeys: String, CodingKey {
        case login
        case id
        case avatarUrlString = "avatar_url"
    }
    
    static func < (lhs: GithubUser, rhs: GithubUser) -> Bool {
        if let lhsLogin = lhs.login, let rhsLogin = rhs.login {
            return lhsLogin < rhsLogin
            
        } else if lhs.login == nil && rhs.login == nil {
            return false
            
        } else if lhs.login == nil {
            return true
            
        } else {
            return false
        }
    }
}
