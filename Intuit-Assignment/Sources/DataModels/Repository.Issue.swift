//
//  Repository.Issue.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Repository {
    
    struct Issue: Equatable, Decodable {
        let number: Int?
        let title: String?
        let body: String?
        let state: State?
        let dateCreated: Date?
        let dateClosed: Date?
        let user: GithubUser?
        let labels: [Label]
        let assignees: [GithubUser]
        let commentCount: Int?
        let commentsUrlString: String?
        
        init(number: Int?,
             title: String?,
             body: String?,
             state: State?,
             dateCreated: Date?,
             dateClosed: Date?,
             user: GithubUser?,
             labels: [Label],
             assignees: [GithubUser],
             commentCount: Int?,
             commentsUrlString: String?) {
            
            self.number = number
            self.title = title
            self.body = body
            self.state = state
            self.dateCreated = dateCreated
            self.dateClosed = dateClosed
            self.user = user
            self.labels = labels
            self.assignees = assignees
            self.commentCount = commentCount
            self.commentsUrlString = commentsUrlString
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            
            number = try container.decodeIfPresent(Int.self, forKey: .number)
            title = try container.decodeIfPresent(String.self, forKey: .title)
            body = try container.decodeIfPresent(String.self, forKey: .body)
            
            if let stateString = try container.decodeIfPresent(String.self, forKey: .state) {
                if stateString.lowercased() == "open" {
                    state = .open(name: stateString)
                } else {
                    state = .other(name: stateString)
                }
            } else {
                state = nil
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            
            if let dateString = try container.decodeIfPresent(String.self, forKey: .dateCreated) {
                dateCreated = dateFormatter.date(from: dateString)
            } else {
                dateCreated = nil
            }
            
            if let dateString = try container.decodeIfPresent(String.self, forKey: .dateClosed) {
                dateClosed = dateFormatter.date(from: dateString)
            } else {
                dateClosed = nil
            }
            
            user = try container.decodeIfPresent(GithubUser.self, forKey: .user)
            labels = try container.decodeIfPresent([Label].self, forKey: .labels) ?? []
            assignees = try container.decodeIfPresent([GithubUser].self, forKey: .assignees) ?? []
            commentCount = try container.decodeIfPresent(Int.self, forKey: .commentCount)
            commentsUrlString = try container.decodeIfPresent(String.self, forKey: .commentsUrlString)
        }
        
        enum CodingKeys: String, CodingKey {
            case number
            case title
            case body
            case state
            case dateCreated = "created_at"
            case user
            case labels
            case assignees
            case commentCount = "comments"
            case dateClosed = "closed_at"
            case commentsUrlString = "comments_url"
        }
    }
}
