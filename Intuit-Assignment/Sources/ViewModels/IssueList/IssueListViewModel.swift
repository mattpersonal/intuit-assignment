//
//  IssueListViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import ReactiveExtras
import RealTalk

/// View Model for the Issue List screen. 
class IssueListViewModel: IssueListViewInput, IssueListViewOutput {
    
    let screenTitle: String?
    let repositoryName: String?
    let viewState: CurrentValueSubject<IssueListViewState, Never>
    let displayedIssueViewModels = PublishedArray<IssueItemViewModel>()
    var showIssueDetailsPublisher: AnyPublisher<(repository: Repository, issue: Repository.Issue), Never> {
        return showIssueDetailSubject.eraseToAnyPublisher()
    }
            
    private let showIssueDetailSubject = PassthroughSubject<(repository: Repository, issue: Repository.Issue), Never>()
    
    /// The unfiltered list of issues that will be filtered to produce the ones that display
    private var allIssueViewModels = [IssueItemViewModel]()
    
    private let issuesService: IssuesServiceApi
    private var fetchIssuesDataTask: URLSessionDataTask?
    private var issueTapCancelBag = CancellableBag()
    
    private let repository: Repository
        
    init(issuesService: IssuesServiceApi = IssuesService(),
         repository: Repository) {
        
        self.issuesService = issuesService
        self.repository = repository
        
        screenTitle = .app(.issueListTitle)
        viewState = CurrentValueSubject(.loading)
        repositoryName = repository.fullName
    }
    
    func loadData() {
        viewState.value = .loading
        
        fetchIssuesDataTask?.cancel()
        fetchIssuesDataTask = issuesService.fetchIssues(from: repository.issuesUrlString,
                                                        networkingStrategy: .fetch,
                                                        completion: { [weak self] fetchResult in
                                                            self?.handleFetchResult(fetchResult)
        })
    }
    
    private func handleFetchResult(_ fetchResult: Result<[Repository.Issue], Networker.NetworkError>) {
        switch fetchResult {
        case let .success(issues):
            allIssueViewModels = createIssueItemViewModels(issues)
            updateDisplayedItems()
            
            if displayedIssueViewModels.elementCount > 0 {
                viewState.value = .displayingData
            } else {
                viewState.value = .emptyData(.app(.issueListEmptyData))
            }
            
        case let .failure(error):
            allIssueViewModels = []
            displayedIssueViewModels.update([])
            viewState.value = .error(error.localizedDescription)
        }
    }
    
    private func createIssueItemViewModels(_ issues: [Repository.Issue]) -> [IssueItemViewModel] {
        return issues.map { issue in
            let itemViewModel = IssueItemViewModel(issue: issue)
            issueTapCancelBag << itemViewModel.userDidTapIssuePublisher
                .sink { [weak self] issue in
                    self?.userDidTapIssue(issue)
            }
            return itemViewModel
        }
    }
    
    private func updateDisplayedItems() {
        displayedIssueViewModels.update(allIssueViewModels)
    }
    
    private func userDidTapIssue(_ issue: Repository.Issue) {
        showIssueDetailSubject.send((repository, issue))
    }
    
    deinit {
        fetchIssuesDataTask?.cancel()
    }
}
