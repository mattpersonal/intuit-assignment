//
//  IssueItemViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

class IssueItemViewModel: IssueItemViewInput, IssueItemViewOutput {
    
    let issueTitle: String?
    let issueSubtitle: String?
    let issueStateName: String?
    let issueStateAccentColor: UIColor?
    let issueStateImage: UIImage?
    let commentCount: Int?
    let commentImage: UIImage?
    
    let userDidTapIssuePublisher = PassthroughSubject<Repository.Issue, Never>()
    
    private let issue: Repository.Issue
    
    init(issue: Repository.Issue) {
        self.issue = issue
        
        issueTitle = issue.title
        issueSubtitle = .app(.issueListItemSubtitle(number: issue.number,
                                                    date: issue.dateCreated,
                                                    userName: issue.user?.login))
        if let state = issue.state {
            issueStateImage = .app(.info)
            
            switch state {
            case let .open(name):
                issueStateName = name
                issueStateAccentColor = .app(.accentPositive)
                
            case let .other(name):
                issueStateName = name
                issueStateAccentColor = .app(.neutralIcon)
            }
        } else {
            issueStateName = nil
            issueStateAccentColor = nil
            issueStateImage = nil
        }
        
        if let commentCount = issue.commentCount {
            self.commentCount = commentCount
            commentImage = .app(.textBubble)
        } else {
            commentCount = nil
            commentImage = nil
        }
    }
    
    func userDidTapIssueItem() {
        userDidTapIssuePublisher.send(issue)
    }
}
