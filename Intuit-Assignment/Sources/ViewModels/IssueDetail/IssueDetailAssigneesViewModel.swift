//
//  IssueDetailAssigneesItemViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

class IssueDetailAssigneesViewModel: IssueDetailAssigneesViewInput {
    
    let title: String?
    let viewState: IssueDetailAssigneesViewState
    let userViewModels: [GithubUserViewModel]
    
    init(issue: Repository.Issue) {
        title = .app(.issueDetailAssignees)
        
        userViewModels = issue.assignees.map { GithubUserViewModel(user: $0,
                                                                   isSelected: false,
                                                                   isSelectable: false) }
        
        if userViewModels.count > 0 {
            viewState = .displayingData
        } else {
            viewState = .emptyData(.app(.issueDetailAssigneesEmptyData))
        }
    }
}
