//
//  IssueDetailNameViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

class IssueDetailNameViewModel: IssueDetailNameViewInput {
    
    let issueName: String?
    let issueNumberString: String?
    
    init(issue: Repository.Issue) {
        issueName = issue.title
        
        if let issueNumber = issue.number {
            issueNumberString = .app(.issueDetailNumber(number: issueNumber))
        } else {
            issueNumberString = nil
        }
    }
}
