//
//  IssueDetailCommentSectionViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import ReactiveExtras
import Combine
import RealTalk

class IssueDetailCommentSectionViewModel {
    
    let header: String?
    let commentViewModels: PublishedArray<IssueDetailCommentRowType>
    
    private var fetchCommentsDataTask: URLSessionDataTask?
    
    private let commentsService: CommentsServiceApi
    private let issue: Repository.Issue
    
    init(commentsService: CommentsServiceApi = CommentsService(),
         issue: Repository.Issue) {
        
        self.commentsService = commentsService
        self.issue = issue
        
        header = .app(.issueDetailCommentsSectionHeader)
        
        if let commentCount = issue.commentCount, commentCount > 0 {
            if issue.commentsUrlString != nil {
                commentViewModels = PublishedArray([.loading])
                loadComments()
            } else {
                commentViewModels = PublishedArray([.simpleMessage(.app(.issueDetailCommentsLoadingError))])
            }
            
        } else {
            commentViewModels = PublishedArray([.simpleMessage(.app(.issueDetailNoComments))])
        }
    }
    
    private func loadComments() {
        commentViewModels.update([.loading])
        
        fetchCommentsDataTask?.cancel()
        fetchCommentsDataTask
            = commentsService.fetchRepositories(from: issue.commentsUrlString,
                                                networkingStrategy: .fetch,
                                                completion: { [weak self] fetchResult in
                                                    self?.handleFetchResult(fetchResult)
            })
    }
    
    private func handleFetchResult(_ fetchResult: Result<[Repository.Issue.Comment], Networker.NetworkError>) {
        switch fetchResult {
        case let .success(comments):
            let commentViewModels: [IssueDetailCommentRowType] = comments.map { .issueComment(IssueDetailCommentViewModel(comment: $0)) }
            self.commentViewModels.update(commentViewModels)
            
        case .failure:
            commentViewModels.update([.simpleMessage(.app(.issueDetailCommentsLoadingError))])
        }
    }
}
