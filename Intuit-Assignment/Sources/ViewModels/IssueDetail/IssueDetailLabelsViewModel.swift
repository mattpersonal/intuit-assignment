//
//  IssueDetailLabelsViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

class IssueDetailLabelsViewModel: IssueDetailLabelsViewInput {
    
    var title: String?
    var labels: [Repository.Issue.Label]
    let viewState: IssueDetailLabelsViewState
    
    init(issue: Repository.Issue) {
        title = .app(.issueDetailLabels)
        labels = issue.labels
        
        if issue.labels.count > 0 {
            viewState = .displayingData
        } else {
            viewState = .emptyData(message: .app(.issueDetailsLabelsEmptyData))
        }
    }
}
