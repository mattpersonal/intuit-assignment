//
//  IssueDetailStatusViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

class IssueDetailStatusViewModel: IssueDetailStatusViewInput {
    
    let statusPrompt: String?
    let status: String?
    let statusAccentColor: UIColor?
    let closedOnString: String?
    let openedOnString: String?
    
    init(issue: Repository.Issue) {
        statusPrompt = .app(.issueDetailStatusPrompt)
        
        if let state = issue.state {
            switch state {
            case let .open(name):
                status = name
                statusAccentColor = .app(.accentPositive)
                
            case let .other(name):
                status = name
                statusAccentColor = .app(.textPrimaryDark)
            }
            
        } else {
            status = .app(.issueDetailUnknownStatus)
            statusAccentColor = .app(.textPrimaryDark)
        }
        
        if let dateClosed = issue.dateClosed {
            closedOnString = .app(.issueDetailClosedOn(date: dateClosed))
        } else {
            closedOnString = nil
        }
        
        openedOnString = String.app(.issueDetailOpenBy(username: issue.user?.login, date: issue.dateCreated)).unwrap
    }
}
