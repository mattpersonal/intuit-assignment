//
//  IssueDetailSummarySectionViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

class IssueDetailSummarySectionViewModel {
    
    let items: [IssueDetailSummarySectionItem]
    
    private let repository: Repository
    private let issue: Repository.Issue
    
    init(repository: Repository, issue: Repository.Issue) {
        self.repository = repository
        self.issue = issue
        
        let nameViewModel = IssueDetailNameViewModel(issue: issue)
        let labelsViewModel = IssueDetailLabelsViewModel(issue: issue)
        let statusViewModel = IssueDetailStatusViewModel(issue: issue)
        let assigneesViewModel = IssueDetailAssigneesViewModel(issue: issue)
        let descriptionViewModel = IssueDetailDescriptionViewModel(issue: issue)
        
        // TODO: add the other items
        
        items = [
            .issueName(viewModel: nameViewModel),
            .labels(viewModel: labelsViewModel),
            .status(viewModel: statusViewModel),
            .assignees(viewModel: assigneesViewModel),
            .description(viewModel: descriptionViewModel)
        ]
    }
}
