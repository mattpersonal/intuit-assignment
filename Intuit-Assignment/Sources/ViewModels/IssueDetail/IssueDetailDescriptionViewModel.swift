//
//  IssueDetailDescriptionItemViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

class IssueDetailDescriptionViewModel: IssueDetailDescriptionViewInput {
    
    let title: String?
    let issueDescription: String?
    
    init(issue: Repository.Issue) {
        title = .app(.issueDetailDescription)
        issueDescription = issue.body
    }
}
