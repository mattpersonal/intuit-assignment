//
//  IssueDetailCommentViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine

class IssueDetailCommentViewModel: IssueDetailCommentViewInput {
    
    let avatarImageState: CurrentValueSubject<DownloadingImageView.State, Never>
    let userName: String?
    let commentDateString: String?
    let commentBody: String?
    
    private var imageLoadingCancellable: AnyCancellable?
    
    init(comment: Repository.Issue.Comment) {
        userName = comment.user?.login
        commentDateString = comment.dateCreated?.toString(format: .full)
        commentBody = comment.body
        
        if let avatarImageUrl = comment.user?.avatarUrlString {
            avatarImageState = CurrentValueSubject(.loading)
            
            // start loading the image
            imageLoadingCancellable = ImageDownloader.download(urlString: avatarImageUrl, completion: { [weak self] image in
                if let image = image {
                    self?.avatarImageState.value = .loaded(image, nil)
                } else {
                    self?.avatarImageState.value = .loaded(.app(.user), .app(.neutralIcon))
                }
            })
            
        } else {
            avatarImageState = CurrentValueSubject(.loaded(.app(.user), .app(.neutralIcon)))
        }
    }
}
