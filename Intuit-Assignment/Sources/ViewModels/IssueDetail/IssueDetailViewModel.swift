//
//  IssueDetailViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import ReactiveExtras

/// View model for the Issue Details screen. 
class IssueDetailViewModel: IssueDetailViewInput, IssueDetailViewOutput {
    
    let screenTitle: String?
    let repositoryName: String?
    let sections: [IssueDetailSection]
    
    private let repository: Repository
    private let issue: Repository.Issue
    
    init(repository: Repository, issue: Repository.Issue) {
        self.repository = repository
        self.issue = issue
        
        screenTitle = .app(.issueDetailTitle)
        repositoryName = repository.fullName
        
        let summaryViewModel = IssueDetailSummarySectionViewModel(repository: repository,
                                                                  issue: issue)
        
        let commentsViewModel = IssueDetailCommentSectionViewModel(issue: issue)
        
        sections = [
            .summary(viewModel: summaryViewModel),
            .comments(viewModel: commentsViewModel)
        ]
    }
}
