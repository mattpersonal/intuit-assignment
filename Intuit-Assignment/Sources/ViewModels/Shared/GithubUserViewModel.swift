//
//  GithubUserViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import ReactiveExtras

class GithubUserViewModel: GithubUserViewInput, GithubUserViewOutput {
    
    let avatarImageState: CurrentValueSubject<DownloadingImageView.State, Never>
    let isSelected: CurrentValueSubject<Bool, Never>
    let username: String?
    
    let userDidTapUserPublisher = PassthroughSubject<GithubUser, Never>()
    
    private let isSelectable: Bool
    
    private var imageLoadingCancellable: AnyCancellable?
    
    private let user: GithubUser
    
    init(user: GithubUser, isSelected: Bool, isSelectable: Bool) {
        self.user = user
        self.isSelectable = isSelectable
        self.isSelected = CurrentValueSubject(isSelectable && isSelected)
        self.username = user.login
        
        if let avatarImageUrl = user.avatarUrlString {
            avatarImageState = CurrentValueSubject(.loading)
            
            // start loading the image
            imageLoadingCancellable = ImageDownloader.download(urlString: avatarImageUrl, completion: { [weak self] image in
                if let image = image {
                    self?.avatarImageState.value = .loaded(image, nil)
                } else {
                    self?.avatarImageState.value = .loaded(.app(.user), .app(.neutralIcon))
                }
            })
            
        } else {
            avatarImageState = CurrentValueSubject(.loaded(.app(.user), .app(.neutralIcon)))
        }
    }
    
    func userDidTapUser() {
        guard isSelectable else {
            return
        }
        
        userDidTapUserPublisher.send(user)
    }
}
