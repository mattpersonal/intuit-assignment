//
//  RepositoryListSectionViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import ReactiveExtras

/// View model representing one section in the repository list.
class RepositoryListSectionViewModel: RepositoryListSectionInput {
        
    /// View model representing the owner of the repositories
    let repositoryOwnerViewModel: RepositoryListOwnerViewModel
    
    /// View model for each repository that will display in this section.
    /// Each view model represents a row in the section.
    let displayedRepositoryViewModels: PublishedArray<RepositoryItemViewModel>
    
    var shouldDisplayHeader: Bool {
        return displayedRepositoryViewModels.elementCount > 0
    }
    
    /// Publisher for when repository items are tapped in this section
    let userTappedRepositoryPublisher = PassthroughSubject<Repository, Never>()
    
    /// The full list of repository view models. We'll use this plus the current search/filtering to populate the displayedRepositoryViewModels.
    private let allRepositoryViewModels: [RepositoryItemViewModel]
    
    private var cancelBag = CancellableBag()
    
    init(owner: GithubUser, repositories: [Repository]) {
        repositoryOwnerViewModel = RepositoryListOwnerViewModel(owner: owner)
        allRepositoryViewModels = repositories.map { RepositoryItemViewModel(repository: $0) }
        displayedRepositoryViewModels = PublishedArray(allRepositoryViewModels)
        
        allRepositoryViewModels.forEach { repoViewModel in
            cancelBag << repoViewModel.userTappedRepositoryItemPublisher.sink { [weak self] repository in
                self?.userTappedRepositoryPublisher.send(repository)
            }
        }
    }
    
    /// Displays on the repositories who's names are prefixed with the search string.
    /// - Parameter search: current search string
    /// - Returns: number of displayed repositories after the update
    func updateDisplayedRepositories(search: String?) -> Int {
        guard let search = search?.unwrap?.lowercased() else {
            // no search - show all
            displayedRepositoryViewModels.update(allRepositoryViewModels)
            return allRepositoryViewModels.count
        }
        
        let filtered = allRepositoryViewModels.filter { repoViewModel -> Bool in
            // check name
            if repoViewModel.repositoryName?.lowercased().contains(search) ?? false {
                return true
            }
            
            // check description
            if repoViewModel.repositoryDescription?.lowercased().contains(search) ?? false {
                return true
            }
            
            return false
        }
        
        displayedRepositoryViewModels.update(filtered)
        return filtered.count
    }
}
