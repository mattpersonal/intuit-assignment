//
//  RepositoryItemViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

class RepositoryItemViewModel: RepositoryItemViewInput, RepositoryItemViewOutput {
    
    let repositoryName: String?
    let repositoryDescription: String?
    let repositoryAccessLevelName: String?
    let repositoryAccessImage: UIImage?
    let isRepositoryPrivate: Bool
    
    /// publisher sending events whenever the user taps on the repository item
    let userTappedRepositoryItemPublisher = PassthroughSubject<Repository, Never>()
    
    private let repository: Repository
    
    init(repository: Repository) {
        self.repository = repository
        
        self.repositoryName = repository.name
        self.repositoryDescription = repository.description
        
        if let isPrivate = repository.isRepositoryPrivate {
            isRepositoryPrivate = isPrivate
            repositoryAccessImage = isPrivate ? .app(.lock) : .app(.lockOpen)
            repositoryAccessLevelName = isPrivate ? .app(.repoListPrivateAccess) : .app(.repoListPublicAccess)
        } else {
            repositoryAccessLevelName = nil
            repositoryAccessImage = nil
            isRepositoryPrivate = false
        }
    }
    
    func userDidTapView() {
        userTappedRepositoryItemPublisher.send(repository)
    }
}
