//
//  RepositoryListViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import ReactiveExtras
import RealTalk

/// View Model for the Repository List. 
class RepositoryListViewModel: RepositoryListViewInput, RepositoryListViewOutput {
    
    // RepositoryListViewInput properties
    let viewState: CurrentValueSubject<RepositoryListViewState, Never>
    let screenTitle: String?
    let sectionViewModels: CurrentValueSubject<[RepositoryListSectionInput], Never>
    lazy var showRepositoryDetailsPublisher: AnyPublisher<Repository, Never> = self.showRepositoryDetailsSubject.eraseToAnyPublisher()
    
    /// Subject used to produce events when a repository detail page should be displayed
    private let showRepositoryDetailsSubject = PassthroughSubject<Repository, Never>()
    
    /// The section view model reprenting repository list sections
    private var backingSectionViewModels: [RepositoryListSectionViewModel] {
        get {
            // not the best - should revisit this to find a better way to get all of this accomplished
            return sectionViewModels.value.map { $0 as! RepositoryListSectionViewModel }
        }
        
        set {
            sectionViewModels.value = newValue
        }
    }
    
    private let repositoriesService: RepositoriesServiceApi
    private var fetchRepoDataTask: URLSessionDataTask?
    private var repositoryTapCancelBag = CancellableBag()
    
    init(repositoriesService: RepositoriesServiceApi = RepositoriesService()) {
        self.repositoriesService = repositoriesService
        screenTitle = .app(.repoListTitle)
        viewState = CurrentValueSubject(.loading)
        sectionViewModels = CurrentValueSubject([])
    }
    
    func loadData(completion: (() -> Void)? = nil) {
        viewState.value = .loading
        
        fetchRepoDataTask?.cancel()
        fetchRepoDataTask
            = repositoriesService.fetchRepositories(from: Settings.repositoriesUrlString,
                                                    networkingStrategy: .fetch,
                                                    completion: { [weak self] fetchResult in
                                                        self?.handleFetchResult(fetchResult)
                                                        completion?()
            })
    }
    
    private func handleFetchResult(_ fetchResult: Result<[Repository], Networker.NetworkError>) {
        switch fetchResult {
        case let .success(repositories):
            sectionViewModels.value = createSectionViewModels(repositories: repositories)
            if repositories.count > 0 {
                viewState.value = .displayingData
            } else {
                viewState.value = .emptyData(.app(.repoListEmptyData))
            }
            
        case .failure:
            sectionViewModels.value = []
            viewState.value = .error(.app(.repoListErrorLoadingData))
        }
    }
    
    func userUpdatedSearch(_ search: String?) {
        let numberDisplayed = backingSectionViewModels.map { $0.updateDisplayedRepositories(search: search) }.reduce(0, +)
        
        if numberDisplayed > 0 {
            viewState.value = .displayingData
        } else {
            viewState.value = .emptyData(.app(.repoListEmptyData))
        }
    }
    
    private func createSectionViewModels(repositories: [Repository]) -> [RepositoryListSectionViewModel] {
        var ownerSections = [GithubUser : [Repository]]()
        
        repositories.forEach { repository in
            guard let owner = repository.owner else {
                return
            }
            
            if ownerSections[owner] != nil {
                ownerSections[owner]?.append(repository)
            } else {
                ownerSections[owner] = [repository]
            }
        }
        
        repositoryTapCancelBag.cancel()
        
        return ownerSections.keys
            .sorted() // use consistent ordering
            .compactMap({ owner -> RepositoryListSectionViewModel? in
                guard let repos = ownerSections[owner] else {
                    return nil
                }
                
                let sectionViewModel = RepositoryListSectionViewModel(owner: owner,
                                                                      repositories: repos)
                
                // observe the taps on repositories
                repositoryTapCancelBag << sectionViewModel.userTappedRepositoryPublisher
                    .sink { [weak self] repository in
                        self?.userDidTapRepository(repository)
                }
                
                return sectionViewModel
            })
    }
    
    private func userDidTapRepository(_ repository: Repository) {
        showRepositoryDetailsSubject.send(repository)
    }
    
    deinit {
        fetchRepoDataTask?.cancel()
    }
}
