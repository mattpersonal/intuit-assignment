//
//  RepositoryListOwnerViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

/// View model representing the owner of a group of repositories. The owner will display as a section header.
class RepositoryListOwnerViewModel: RepositoryListOwnerHeaderViewInput {
    let avatarImageState: CurrentValueSubject<DownloadingImageView.State, Never>
    let ownerPrompt: String?
    let ownerName: String?
    
    private var imageLoadingCancellable: AnyCancellable?
    
    init(owner: GithubUser) {
        ownerPrompt = .app(.repoListOwner)
        ownerName = owner.login
        
        if let avatarUrl = owner.avatarUrlString {
            avatarImageState = CurrentValueSubject(.loading)

            // start loading the image
            imageLoadingCancellable = ImageDownloader.download(urlString: avatarUrl, completion: { [weak self] image in
                if let image = image {
                    self?.avatarImageState.value = .loaded(image, nil)
                } else {
                    self?.avatarImageState.value = .hidden
                }
            })
            
        } else {
            avatarImageState = CurrentValueSubject(.hidden)
        }
    }
}
