//
//  RepositoryDetailSectionViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import ReactiveExtras

class RepositoryDetailSectionViewModel {
    
    let itemDisplayStyle: RepositoryDetailItemDisplayStyle
    let itemViewModels: [RepositoryDetailItemViewModel]
    
    init(itemDisplayStyle: RepositoryDetailItemDisplayStyle, itemViewModels: [RepositoryDetailItemViewModel]) {
        self.itemDisplayStyle = itemDisplayStyle
        self.itemViewModels = itemViewModels
    }
}
