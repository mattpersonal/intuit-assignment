//
//  RepositoryDetailItemViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

protocol RepositoryDetailItemViewInput {
    var accentImage: UIImage? { get }
    var title: String? { get }
    var count: Int? { get }
    var isTappable: Bool { get }
}

protocol RepositoryDetailItemViewOutput {
    func userDidTapItem()
}

class RepositoryDetailItemViewModel: RepositoryDetailItemViewInput, RepositoryDetailItemViewOutput {
    
    let accentImage: UIImage?
    let title: String?
    let count: Int?
    let isTappable: Bool
    
    let userDidTapItemPublisher = PassthroughSubject<RepositoryDetailItem, Never>()

    private let item: RepositoryDetailItem
    
    init(item: RepositoryDetailItem, accentImage: UIImage?, title: String?, count: Int?, isTappable: Bool) {
        self.item = item
        self.accentImage = accentImage
        self.title = title
        self.count = count
        self.isTappable = isTappable
    }
    
    func userDidTapItem() {
        guard isTappable else {
            return // shouldn't have even been called, but just be safe
        }
        userDidTapItemPublisher.send(item)
    }
}
