//
//  RepositoryDetailViewModel.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import ReactiveExtras

enum RepositoryDetailItem: CaseIterable {
    case stars
    case watchers
    case forks
    case issues
    case contributors
    case license
    
    var displayStyle: RepositoryDetailItemDisplayStyle {
        switch self {
        case .stars, .watchers, .forks, .issues:
            return .small
            
        case .contributors, .license:
            return .large
        }
    }
}

/// View Model for the Repository Details screen. 
class RepositoryDetailViewModel: RepositoryDetailViewInput, RepositoryDetailViewOutput {
    
    let screenTitle: String?
    let repositoryName: String?
    let ownerNameString: String?
    let repositoryDescription: String?
    let sectionViewModels: [RepositoryDetailSectionViewModel]
    
    var showIssuesListPublisher: AnyPublisher<Repository, Never> {
        return showIssuesListSubject.eraseToAnyPublisher()
    }
    
    private let showIssuesListSubject = PassthroughSubject<Repository, Never>()
    
    private var sectionViewModelCancelBag = CancellableBag()
    
    private let repository: Repository
    
    init(repository: Repository) {
        self.repository = repository
        
        screenTitle = .app(.repoDetailTitle)
        repositoryName = repository.name
        if let ownerName = repository.owner?.login?.unwrap {
            ownerNameString = .app(.repoDetailOwner(ownerName: ownerName))
        } else {
            ownerNameString = nil
        }
        repositoryDescription = repository.description
        
        var largeDisplayItemViewModels = [RepositoryDetailItemViewModel]()
        var smallDisplayItemViewModels = [RepositoryDetailItemViewModel]()
        
        RepositoryDetailItem.allCases.forEach { item in
            let itemViewModel: RepositoryDetailItemViewModel
            
            switch item {
            case .stars:
                itemViewModel = RepositoryDetailItemViewModel(item: item,
                                                              accentImage: .app(.star),
                                                              title: .app(.repoDetailStars),
                                                              count: repository.starCount,
                                                              isTappable: false)
                
            case .watchers:
                itemViewModel = RepositoryDetailItemViewModel(item: item,
                                                              accentImage: .app(.eye),
                                                              title: .app(.repoDetailWatchers),
                                                              count: repository.watcherCount,
                                                              isTappable: false)
                
            case .forks:
                itemViewModel = RepositoryDetailItemViewModel(item: item,
                                                              accentImage: .app(.tuningFork),
                                                              title: .app(.repoDetailForks),
                                                              count: repository.forkCount,
                                                              isTappable: false)
                
            case .issues:
                let isTappable = repository.openIssuesCount ?? 0 > 0
                itemViewModel = RepositoryDetailItemViewModel(item: item,
                                                              accentImage: .app(.warning),
                                                              title: .app(.repoDetailIssues),
                                                              count: repository.openIssuesCount,
                                                              isTappable: isTappable)
                
            case .contributors:
                return // Viewing contributors is not implemented yet. This would just show a cell that says contributors but it would not be able to take you to a page that shows contributors. Alternatively, we could just show them in the cell. Either way, did not have time.
                
            case .license:
                itemViewModel = RepositoryDetailItemViewModel(item: item,
                                                              accentImage: .app(.document),
                                                              title: repository.license?.name,
                                                              count: nil,
                                                              isTappable: false)
            }
            
            // add the view model to the right section
            switch item.displayStyle {
            case .small:
                smallDisplayItemViewModels.append(itemViewModel)
                
            case .large:
                largeDisplayItemViewModels.append(itemViewModel)
            }
        }
        
        let largeSection = RepositoryDetailSectionViewModel(itemDisplayStyle: .large,
                                                            itemViewModels: largeDisplayItemViewModels)
        let smallSection = RepositoryDetailSectionViewModel(itemDisplayStyle: .small,
                                                            itemViewModels: smallDisplayItemViewModels)
        sectionViewModels = [smallSection, largeSection]
        
        // bind to taps for the items
        for sectionViewModel in sectionViewModels {
            for itemViewModel in sectionViewModel.itemViewModels {
                sectionViewModelCancelBag << itemViewModel.userDidTapItemPublisher
                    .sink { [weak self] item in
                        self?.userDidTapItem(item)
                }
            }
        }
    }
    
    private func userDidTapItem(_ item: RepositoryDetailItem) {
        switch item {
        case .issues:
            showIssuesListSubject.send(repository)
            
        case .contributors, .forks, .license, .stars, .watchers:
            return // do nothing - should not have receive this anyway
        }
    }
}
