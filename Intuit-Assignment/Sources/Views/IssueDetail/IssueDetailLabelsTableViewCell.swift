//
//  IssueDetailLabelsTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

protocol IssueDetailLabelsViewInput {
    var title: String? { get }
    var labels: [Repository.Issue.Label] { get }
    var viewState: IssueDetailLabelsViewState { get }
}

enum IssueDetailLabelsViewState {
    case displayingData
    case emptyData(message: String?)
}

class IssueDetailLabelsTableViewCell: UITableViewCell {
    
    @IBOutlet private var rootView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var collectionViewHeightConstraint: NSLayoutConstraint!
    
    private var viewModel: IssueDetailLabelsViewInput?
    
    // this keeps track of if the user has scrolled so that we can stop animating the cells in
    private var hasUserScrolledAfterDataReload = false
        
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("IssueDetailLabelsView", owner: self, options: nil)
        contentView.fill(with: rootView,
                         top: 8,
                         trailing: 16,
                         bottom: 8,
                         leading: 16)
        
        styleUI()
        setupCollectionView()
    }
    
    private func styleUI() {
        backgroundColor = .clear
        contentView.setShadow(spec: .default)
        contentView.backgroundColor = .clear
        rootView.backgroundColor = .app(.backgroundCard)
        rootView.layer.cornerRadius = Dimensions.cardCornerRadius
        
        titleLabel.textColor = .app(.textPrimaryDark)
        titleLabel.font = .app(.listItemTitle)
        errorLabel.textColor = .app(.textSecondaryDark)
        errorLabel.font = .app(.listItemBody)
        
    }
    
    private func setupCollectionView() {
        collectionView.backgroundColor = .clear

        let insets = UIEdgeInsets(top: 8,
                                  left: 16,
                                  bottom: 8,
                                  right: 16)
        collectionView.contentInset = insets
        collectionViewHeightConstraint.constant = insets.top + insets.bottom + IssueDetailLabelCollectionViewCell.desiredHeight()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = Dimensions.standardPadding
        flowLayout.scrollDirection = .horizontal
        
        collectionView.register(IssueDetailLabelCollectionViewCell.self,
                                forCellWithReuseIdentifier: IssueDetailLabelCollectionViewCell.reuseIdentifier)
        
        collectionView.collectionViewLayout = flowLayout
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func bind(to viewModel: IssueDetailLabelsViewInput) {
        self.viewModel = viewModel
        
        titleLabel.setTextOrHide(viewModel.title)
        
        switch viewModel.viewState {
        case .displayingData:
            // reload the collection view
            collectionView.isHidden = false
            hasUserScrolledAfterDataReload = true
            collectionView.reloadData()
            errorLabel.isHidden = true
            
        case let .emptyData(message):
            collectionView.isHidden = true
            errorLabel.setTextOrHide(message)
        }
    }
}

extension IssueDetailLabelsTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let viewModel = viewModel else {
            return .zero
        }
        
        let height = IssueDetailLabelCollectionViewCell.desiredHeight()
        let width = IssueDetailLabelCollectionViewCell.width(labelName: viewModel.labels[indexPath.item].name)
        
        return CGSize(width: width, height: height)
    }
}

extension IssueDetailLabelsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.labels.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let viewModel = viewModel, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IssueDetailLabelCollectionViewCell.reuseIdentifier, for: indexPath) as? IssueDetailLabelCollectionViewCell else {
            return UICollectionViewCell()
        }
                
        cell.set(label: viewModel.labels[indexPath.item])
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hasUserScrolledAfterDataReload = true
    }
}
