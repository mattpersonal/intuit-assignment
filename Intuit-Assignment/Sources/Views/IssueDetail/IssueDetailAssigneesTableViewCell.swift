//
//  IssueDetailAssigneesTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

protocol IssueDetailAssigneesViewInput {
    var title: String? { get }
    var viewState:  IssueDetailAssigneesViewState { get }
    var userViewModels: [GithubUserViewModel] { get }
}

enum IssueDetailAssigneesViewState {
    case displayingData
    case emptyData(String?)
}

class IssueDetailAssigneesTableViewCell: UITableViewCell {

    @IBOutlet private var rootView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var collectionViewHeightConstraint: NSLayoutConstraint!
    
    private var viewModel: IssueDetailAssigneesViewInput?
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("IssueDetailAssigneesView", owner: self, options: nil)
        contentView.fill(with: rootView,
                         top: 8,
                         trailing: 16,
                         bottom: 8,
                         leading: 16)
        styleUI()
        setupCollectionView()
    }
    
    private func styleUI() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.setShadow(spec: .default)
        rootView.backgroundColor = .app(.backgroundCard)
        rootView.layer.cornerRadius = Dimensions.cardCornerRadius
        
        titleLabel.textColor = .app(.textPrimaryDark)
        titleLabel.font = .app(.listItemTitle)
        errorLabel.textColor = .app(.textSecondaryDark)
        errorLabel.font = .app(.listItemBody)
    }
    
    private func setupCollectionView() {
        collectionView.backgroundColor = .clear
        
        let insets = UIEdgeInsets(top: 8,
                                  left: 16,
                                  bottom: 8,
                                  right: 16)
        collectionView.contentInset = insets
        collectionViewHeightConstraint.constant = insets.top + insets.bottom + IssueDetailAssigneeCollectionViewCell.desiredHeight()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = Dimensions.standardPadding
        flowLayout.scrollDirection = .horizontal
        
        collectionView.register(IssueDetailAssigneeCollectionViewCell.self,
                                forCellWithReuseIdentifier: IssueDetailAssigneeCollectionViewCell.reuseIdentifier)
        
        collectionView.collectionViewLayout = flowLayout
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func bind(to viewModel: IssueDetailAssigneesViewInput) {
        self.viewModel = viewModel
        
        titleLabel.setTextOrHide(viewModel.title)
        
        switch viewModel.viewState {
        case .displayingData:
            // reload the collection view
            collectionView.isHidden = false
            collectionView.reloadData()
            errorLabel.isHidden = true
            
        case let .emptyData(message):
            collectionView.isHidden = true
            errorLabel.setTextOrHide(message)
        }
    }
}

extension IssueDetailAssigneesTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let viewModel = viewModel else {
            return .zero
        }
        
        let height = IssueDetailAssigneeCollectionViewCell.desiredHeight()
        let width = IssueDetailAssigneeCollectionViewCell.width(username: viewModel.userViewModels[indexPath.item].username)
        
        return CGSize(width: width, height: height)
    }
}

extension IssueDetailAssigneesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.userViewModels.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let viewModel = viewModel, let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IssueDetailAssigneeCollectionViewCell.reuseIdentifier, for: indexPath) as? IssueDetailAssigneeCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.bind(to: viewModel.userViewModels[indexPath.item])
        return cell
    }
}
