//
//  IssueDetailLabelCollectionViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

class IssueDetailLabelCollectionViewCell: UICollectionViewCell {
    
    private let stackView = UIStackView()
    private let colorView = UIView()
    private let nameLabel = UILabel()
    
    private static let colorIndicatorDiameter: CGFloat = 12
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        contentView.backgroundColor = .clear
        
        // configure stackview
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8
        contentView.fill(with: stackView)
        
        // configure color view
        colorView.widthAnchor.constraint(equalToConstant: IssueDetailLabelCollectionViewCell.colorIndicatorDiameter).isActive = true
        colorView.heightAnchor.constraint(equalToConstant: IssueDetailLabelCollectionViewCell.colorIndicatorDiameter).isActive = true
        colorView.layer.cornerRadius = IssueDetailLabelCollectionViewCell.colorIndicatorDiameter / 2
        stackView.addArrangedSubview(colorView)
        
        // configure the label
        nameLabel.textColor = .app(.textPrimaryDark)
        nameLabel.font = .app(.listItemBody)
        stackView.addArrangedSubview(nameLabel)
    }
    
    func set(label: Repository.Issue.Label) {
        nameLabel.setTextOrHide(label.name)
        colorView.backgroundColor = label.color
    }
    
    static func desiredHeight() -> CGFloat {
        let heightColorView = IssueDetailLabelCollectionViewCell.colorIndicatorDiameter
        let heightLabel = "Mock String".height(withConstrainedWidth: .greatestFiniteMagnitude, font: .app(.listItemBody))
        
        return max(heightColorView, heightLabel)
    }
    
    static func width(labelName: String?) -> CGFloat {
        let colorViewWidth = IssueDetailLabelCollectionViewCell.colorIndicatorDiameter
        let stackViewSpacing: CGFloat = 8
        let labelWidth = labelName?.width(withConstrainedHeight: .greatestFiniteMagnitude, font: .app(.listItemBody)) ?? CGFloat(0)
        
        return colorViewWidth + stackViewSpacing + labelWidth
    }
}
