//
//  IssueDetailStatusTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

protocol IssueDetailStatusViewInput {
    var statusPrompt: String? { get }
    var status: String? { get }
    var statusAccentColor: UIColor? { get }
    var closedOnString: String? { get }
    var openedOnString: String? { get }
}

class IssueDetailStatusTableViewCell: UITableViewCell {
    
    @IBOutlet private var rootView: UIView!
    @IBOutlet private var statusPromptLabel: UILabel!
    @IBOutlet private var statusLabel: UILabel!
    @IBOutlet private var closedOnLabel: UILabel!
    @IBOutlet private var openedLabel: UILabel!
    
    private var viewModel: IssueDetailStatusViewInput?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("IssueDetailStatusView", owner: self, options: nil)
        contentView.fill(with: rootView,
                         top: 8,
                         trailing: 16,
                         bottom: 8,
                         leading: 16)
        styleUI()
    }
    
    private func styleUI() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.setShadow(spec: .default)
        rootView.backgroundColor = .app(.backgroundCard)
        rootView.layer.cornerRadius = Dimensions.cardCornerRadius
        
        statusPromptLabel.textColor = .app(.textPrimaryDark)
        statusPromptLabel.font = .app(.listItemTitleLight)
        statusLabel.font = .app(.listItemTitle)
        closedOnLabel.font = .app(.listItemBody)
        closedOnLabel.textColor = .app(.textSecondaryDark)
        openedLabel.font = .app(.listItemBody)
        openedLabel.textColor = .app(.textSecondaryDark)
    }

    func bind(to viewModel: IssueDetailStatusViewInput) {
        self.viewModel = viewModel
        
        statusPromptLabel.setTextOrHide(viewModel.statusPrompt)
        statusLabel.setTextOrHide(viewModel.status)
        statusLabel.textColor = viewModel.statusAccentColor
        closedOnLabel.setTextOrHide(viewModel.closedOnString)
        openedLabel.setTextOrHide(viewModel.openedOnString)
    }
}
