//
//  IssueDetailNameTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

protocol IssueDetailNameViewInput {
    var issueName: String? { get }
    var issueNumberString: String? { get }
}

class IssueDetailNameTableViewCell: UITableViewCell {
    
    @IBOutlet private var rootView: UIView!
    @IBOutlet private var issueNameLabel: UILabel!
    @IBOutlet private var issueNumberLabel: UILabel!
    
    private var viewModel: IssueDetailNameViewInput?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("IssueDetailNameView", owner: self, options: nil)
        contentView.fill(with: rootView)
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        rootView.backgroundColor = .clear
        
        issueNameLabel.textColor = .app(.textPrimaryDark)
        issueNameLabel.font = .app(.screenHeaderTitlePrimary)
        issueNumberLabel.textColor = .app(.textPrimaryDark)
        issueNumberLabel.font = .app(.screenHeaderTitleSecondary)
    }
    
    func bind(to viewModel: IssueDetailNameViewInput) {
        self.viewModel = viewModel
        
        issueNameLabel.setTextOrHide(viewModel.issueName)
        issueNumberLabel.setTextOrHide(viewModel.issueNumberString)
    }
}
