//
//  IssueDetailDescriptionTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

protocol IssueDetailDescriptionViewInput {
    var title: String? { get }
    var issueDescription: String? { get }
}

class IssueDetailDescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet var rootView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    private var viewModel: IssueDetailDescriptionViewInput?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("IssueDetailDescriptionView", owner: self, options: nil)
        contentView.fill(with: rootView,
                         top: 8,
                         trailing: 16,
                         bottom: 8,
                         leading: 16)
        styleUI()
    }
    
    private func styleUI() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.setShadow(spec: .default)
        rootView.backgroundColor = .app(.backgroundCard)
        rootView.layer.cornerRadius = Dimensions.cardCornerRadius
        
        titleLabel.textColor = .app(.textPrimaryDark)
        titleLabel.font = .app(.listItemTitle)
        descriptionLabel.textColor = .app(.textPrimaryDark)
        descriptionLabel.font = .app(.listItemBody)
    }

    func bind(to viewModel: IssueDetailDescriptionViewInput) {
        self.viewModel = viewModel
        
        titleLabel.setTextOrHide(viewModel.title)
        descriptionLabel.setTextOrHide(viewModel.issueDescription)
    }
}
