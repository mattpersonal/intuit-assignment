//
//  IssueDetailCommentTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

protocol IssueDetailCommentViewInput {
    var avatarImageState: CurrentValueSubject<DownloadingImageView.State, Never> { get }
    var userName: String? { get }
    var commentDateString: String? { get }
    var commentBody: String? { get }
}

class IssueDetailCommentTableViewCell: UITableViewCell {
    
    @IBOutlet var rootView: UIView!
    @IBOutlet var avatarImageView: DownloadingImageView!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var commentDateLabel: UILabel!
    @IBOutlet var separatorView: UIView!
    @IBOutlet var commentBodyLabel: UILabel!
    @IBOutlet var avatarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var avatarWidthConstraint: NSLayoutConstraint!
    
    private var viewModel: IssueDetailCommentViewInput?
    
    private var viewModelCanceBag = CancellableBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("IssueDetailCommentView", owner: self, options: nil)
        contentView.fill(with: rootView,
                         top: 8,
                         trailing: 16,
                         bottom: 8,
                         leading: 16)
        
        styleUI()
        
        accessibilityIdentifier = "IssueDetailCommentTableViewCell"
    }
    
    private func styleUI() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.setShadow(spec: .default)
        rootView.backgroundColor = .app(.backgroundCard)
        rootView.layer.cornerRadius = Dimensions.cardCornerRadius
        
        avatarHeightConstraint.constant = Dimensions.avatarSize.height
        avatarWidthConstraint.constant = Dimensions.avatarSize.width
        avatarImageView.layer.cornerRadius = Dimensions.avatarCornerRadius
        avatarImageView.clipsToBounds = true
        
        userNameLabel.textColor = .app(.textPrimaryDark)
        userNameLabel.font = .app(.listItemTitle)
        commentDateLabel.textColor = .app(.textSecondaryDark)
        commentDateLabel.font = .app(.listItemSubtitle)
        commentBodyLabel.textColor = .app(.textPrimaryDark)
        commentBodyLabel.font = .app(.listItemBody)
        
        separatorView.backgroundColor = .app(.separator)
    }

    func bind(to viewModel: IssueDetailCommentViewInput) {
        self.viewModel = viewModel
        
        viewModelCanceBag.cancel()
        
        userNameLabel.setTextOrHide(viewModel.userName)
        commentDateLabel.setTextOrHide(viewModel.commentDateString)
        commentBodyLabel.setTextOrHide(viewModel.commentBody)
        
        viewModelCanceBag << viewModel.avatarImageState
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] state in
                self?.avatarImageView.setState(state)
            })
    }
}
