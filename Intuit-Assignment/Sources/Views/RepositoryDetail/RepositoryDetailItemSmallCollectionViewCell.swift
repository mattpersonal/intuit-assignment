//
//  RepositoryDetailItemSmallCollectionViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

class RepositoryDetailItemSmallCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var rootView: UIView!
    @IBOutlet var mainContentView: UIView!
    @IBOutlet var accentImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var disclosureImageView: UIImageView!
    
    private var viewModel: (RepositoryDetailItemViewInput & RepositoryDetailItemViewOutput)?
    
    private var viewModelCancelBag = CancellableBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("RepositoryDetailItemSmallView", owner: self, options: nil)
        fill(with: rootView)
        
        styleUI()
        
        accessibilityIdentifier = "RepositoryDetailItemSmallCollectionViewCell"
    }
    
    private func styleUI() {
        backgroundColor = .clear
        rootView.backgroundColor = .clear
        rootView.setShadow(spec: .default)
        mainContentView.backgroundColor = .app(.backgroundCard)
        mainContentView.layer.cornerRadius = Dimensions.cardCornerRadius
        disclosureImageView.setImageColor(.app(.neutralIcon))
        
        titleLabel.textColor = .app(.textPrimaryDark)
        countLabel.textColor = .app(.textPrimaryDark)
        titleLabel.font = .app(.body)
        countLabel.font = .app(.listItemTitle)
    }
    
    func bind(to viewModel: RepositoryDetailItemViewInput & RepositoryDetailItemViewOutput) {
        self.viewModel = viewModel
        
        viewModelCancelBag.cancel()
        
        if let accentImage = viewModel.accentImage {
            accentImageView.image = accentImage
            accentImageView.setImageColor(.app(.accentColorForLightBackground))
            accentImageView.alpha = 1
        } else {
            accentImageView.alpha = 0
        }
        
        titleLabel.text = viewModel.title // don't hide for empty. use as placeholder
        
        let countString: String?
        if let count = viewModel.count {
            countString = String(count)
        } else {
            countString = nil
        }
        countLabel.text = countString // don't hide for empty. use as placeholder
        
        if viewModel.isTappable {
            disclosureImageView.alpha = 1
            
            // conditionally add tap
            viewModelCancelBag << mainContentView.tapGesturePublisher
                .receive(on: DispatchQueue.main)
                .sink { [weak self] _ in
                    self?.viewModel?.userDidTapItem()
            }
            
        } else {
            disclosureImageView.alpha = 0
        }
    }
}
