//
//  IssueItemTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import ReactiveExtras

protocol IssueItemViewInput {
    var issueTitle: String? { get }
    var issueSubtitle: String? { get }
    var issueStateName: String? { get }
    var issueStateAccentColor: UIColor? { get }
    var issueStateImage: UIImage? { get }
    var commentCount: Int? { get }
    var commentImage: UIImage? { get }
}

protocol IssueItemViewOutput {
    func userDidTapIssueItem()
}

class IssueItemTableViewCell: UITableViewCell {
    
    @IBOutlet private var rootView: UIView!
    @IBOutlet private var issueTitleLabel: UILabel!
    @IBOutlet private var issueSubtitleLabel: UILabel!
    @IBOutlet private var issueStateStackview: UIStackView!
    @IBOutlet private var issueStateLabel: UILabel!
    @IBOutlet private var issueStateImageView: UIImageView!
    @IBOutlet private var commentCountLabel: UILabel!
    @IBOutlet private var commentImageView: UIImageView!
    @IBOutlet private var commentCountStackView: UIStackView!
    
    private var viewModel: (IssueItemViewInput & IssueItemViewOutput)?
    
    private var viewModelCancelBag = CancellableBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("IssueItemView", owner: self, options: nil)
        contentView.fill(with: rootView,
                         top: 2,
                         trailing: 0,
                         bottom: 0,
                         leading: 0)
        
        styleUI()
        
        accessibilityIdentifier = "IssueItemTableViewCell"
    }
    
    private func styleUI() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        rootView.backgroundColor = .app(.backgroundCard)
        
        issueTitleLabel.textColor = .app(.textPrimaryDark)
        issueTitleLabel.font = .app(.listItemTitle)
        issueSubtitleLabel.textColor = .app(.textSecondaryLight)
        issueSubtitleLabel.font = .app(.listItemBody)
        
        issueStateLabel.font = .app(.caption)
        commentCountLabel.font = .app(.caption)
        commentCountLabel.textColor = .app(.textPrimaryDark)
    }
    
    func bind(to viewModel: IssueItemViewInput & IssueItemViewOutput) {
        self.viewModel = viewModel
        
        viewModelCancelBag.cancel()
        
        issueTitleLabel.setTextOrHide(viewModel.issueTitle)
        issueSubtitleLabel.setTextOrHide(viewModel.issueSubtitle)
        
        if let stateString = viewModel.issueStateName?.unwrap {
            issueStateStackview.isHidden = false
            issueStateLabel.text = stateString
            issueStateLabel.textColor = viewModel.issueStateAccentColor
            issueStateImageView.image = viewModel.issueStateImage
            issueStateImageView.setImageColor(viewModel.issueStateAccentColor)
            
        } else {
            issueStateStackview.isHidden = true
        }
        
        if let commentCount = viewModel.commentCount {
            commentCountStackView.isHidden = false
            commentCountLabel.text = String(commentCount)
            commentImageView.image = viewModel.commentImage
            commentImageView.setImageColor(.app(.neutralIcon))
            
        } else {
            commentCountStackView.isHidden = true
        }
        
        viewModelCancelBag << rootView.tapGesturePublisher.sink { [weak self] _ in
            self?.viewModel?.userDidTapIssueItem()
        }
    }
}
