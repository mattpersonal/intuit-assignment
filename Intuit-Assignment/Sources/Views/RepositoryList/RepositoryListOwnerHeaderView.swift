//
//  RepositoryListOwnerHeaderView.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

protocol RepositoryListOwnerHeaderViewInput {
    var avatarImageState: CurrentValueSubject<DownloadingImageView.State, Never> { get }
    var ownerPrompt: String? { get }
    var ownerName: String? { get }
}

class RepositoryListOwnerHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet private var rootView: UIView!
    @IBOutlet private var backgroundBlurView: UIVisualEffectView!
    @IBOutlet private var avatarImageView: DownloadingImageView!
    @IBOutlet private var ownerPromptLabel: UILabel!
    @IBOutlet private var ownerNameLabel: UILabel!
    
    private var viewModel: RepositoryListOwnerHeaderViewInput?
    
    private var cancelBag = CancellableBag()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("RepositoryListOwnerHeaderView", owner: self, options: nil)
        fill(with: rootView)
        
        styleUI()
    }
    
    private func styleUI() {
        rootView.backgroundColor = .clear
        
        // hide blur initially. Blur will only be shown when this view is serving as a sticky header.
        backgroundBlurView.alpha = 0
        
        ownerPromptLabel.textColor = .app(.textSecondaryDark)
        ownerPromptLabel.font = .app(.caption)
        ownerNameLabel.textColor = .app(.textPrimaryDark)
        ownerNameLabel.font = .app(.sectionHeaderTitle)
    }
    
    func bind(to viewModel: RepositoryListOwnerHeaderViewInput) {
        self.viewModel = viewModel
        
        cancelBag.cancel()
        
        cancelBag << viewModel.avatarImageState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                self?.avatarImageView.setState(state)
            }
        
        ownerPromptLabel.setTextOrHide(viewModel.ownerPrompt)
        ownerNameLabel.setTextOrHide(viewModel.ownerName)
    }
    
    func setBlurHidden(_ isHidden: Bool) {
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.backgroundBlurView.alpha = isHidden ? 0 : 1
        }
    }
}
