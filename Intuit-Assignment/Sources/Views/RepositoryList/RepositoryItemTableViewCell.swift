//
//  RepositoryItemTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import ReactiveExtras

protocol RepositoryItemViewInput {
    var repositoryName: String? { get }
    var repositoryDescription: String? { get }
    var repositoryAccessLevelName: String? { get }
    var repositoryAccessImage: UIImage? { get }
    var isRepositoryPrivate: Bool { get }
}

protocol RepositoryItemViewOutput {
    func userDidTapView()
}

class RepositoryItemTableViewCell: UITableViewCell {

    @IBOutlet private var rootView: UIView!
    @IBOutlet private var rightArrowImageView: UIImageView!
    @IBOutlet private var repositoryNameLabel: UILabel!
    @IBOutlet private var repositoryDescriptionLabel: UILabel!
    @IBOutlet private var accessLevelImageView: UIImageView!
    @IBOutlet private var accessLevelNameLabel: UILabel!
    
    private var viewModel: (RepositoryItemViewInput & RepositoryItemViewOutput)?
    
    private var cancelBag = CancellableBag()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("RepositoryItemView", owner: self, options: nil)
        contentView.fill(with: rootView,
                         top: 2,
                         trailing: 0,
                         bottom: 0,
                         leading: 0)
        
        styleUI()
        
        // set the accessibility identifier for easier UI testing
        accessibilityIdentifier = "RepositoryItemTableViewCell"
    }
    
    private func styleUI() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        rootView.backgroundColor = .app(.backgroundCard)
        
        repositoryNameLabel.font = .app(.listItemTitle)
        repositoryNameLabel.textColor = .app(.textPrimaryDark)
        
        repositoryDescriptionLabel.font = .app(.listItemBody)
        repositoryDescriptionLabel.textColor = .app(.textPrimaryDark)
        
        accessLevelNameLabel.font = .app(.caption)
        
        rightArrowImageView.setImageColor(.app(.neutralIcon))
    }
    
    func bind(to viewModel: (RepositoryItemViewInput & RepositoryItemViewOutput)?) {
        self.viewModel = viewModel
        
        // cancel any previous subscriptions before resubscribing
        cancelBag.cancel()
        
        repositoryNameLabel.setTextOrHide(viewModel?.repositoryName)
        repositoryDescriptionLabel.setTextOrHide(viewModel?.repositoryDescription)
        accessLevelNameLabel.setTextOrHide(viewModel?.repositoryAccessLevelName)
        accessLevelImageView.image = viewModel?.repositoryAccessImage
        accessLevelImageView.isHidden = viewModel?.repositoryAccessImage == nil
        
        if let isRepositoryPrivate = viewModel?.isRepositoryPrivate {
            let accessLevelAccentColor: UIColor =
                isRepositoryPrivate ? .app(.accentNegative) : .app(.accentPositive)
            accessLevelNameLabel.textColor = accessLevelAccentColor
            accessLevelImageView.setImageColor(accessLevelAccentColor)
        }
        
        // subscribe to taps on the entire view
        cancelBag << rootView.tapGesturePublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.viewModel?.userDidTapView()
            }
    }
}
