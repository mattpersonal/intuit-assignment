//
//  LoadingTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {

    private let loadingIndicator = UIActivityIndicatorView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
                
        // enable autolayout
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        // add view
        addSubview(loadingIndicator)
        
        // constrain the view
        loadingIndicator.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        loadingIndicator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
        loadingIndicator.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        loadingIndicator.widthAnchor.constraint(equalToConstant: 24).isActive = true
        loadingIndicator.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        loadingIndicator.startAnimating()
    }
}
