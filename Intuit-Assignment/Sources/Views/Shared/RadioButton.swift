//
//  RadioButton.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

class RadioButton: UIView {
    
    private let outlineView = UIView()
    private let fillingView = UIView()
    
    /// Publisher producing events when the user toggles the radio button. The Bool is true if the radio button is now selected.
    let userToggledSelectionPublisher = PassthroughSubject<Bool, Never>()
    
    let isSelectedSubject = CurrentValueSubject<Bool, Never>(false)
    
    var accentColor: UIColor? {
        didSet {
            outlineView.layer.borderColor = accentColor?.cgColor
            fillingView.backgroundColor = accentColor
        }
    }
    
    private var cancelBag = CancellableBag()

    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let cornerRadius = min(frame.height, frame.width) / 2
        outlineView.layer.cornerRadius = cornerRadius
        outlineView.layer.borderWidth = 2
        fillingView.layer.cornerRadius = cornerRadius
    }
    
    private func commonInit() {
        backgroundColor = .clear
        
        fill(with: outlineView)
        fill(with: fillingView)
        
        cancelBag << isSelectedSubject.receive(on: DispatchQueue.main).sink(receiveValue: { [weak self] isSelected in
            self?.setSelected(isSelected)
        })
        
        cancelBag << tapGesturePublisher.receive(on: DispatchQueue.main).sink(receiveValue: { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            
            // toggle on tap
            strongSelf.isSelectedSubject.value = !strongSelf.isSelectedSubject.value
            
            // alert subscribers
            strongSelf.userToggledSelectionPublisher.send(strongSelf.isSelectedSubject.value)
        })
    }
    
    private func setSelected(_ selected: Bool) {
        let finalFillingTransform: CGAffineTransform
        
        if selected {
            finalFillingTransform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        } else {
            finalFillingTransform = CGAffineTransform(scaleX: 0, y: 0)
        }
        
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.fillingView.transform = finalFillingTransform
            self?.setNeedsLayout()
            self?.layoutIfNeeded()
        }
    }
}
