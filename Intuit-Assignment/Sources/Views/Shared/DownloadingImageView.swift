//
//  DownloadingImageView.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

/// Downloading image view can display in different states based on whether or not the image is loading.
class DownloadingImageView: UIView {
    
    enum State {
        case loading
        case loaded(UIImage?, UIColor?)
        case hidden // hidden using isHidden
        case invisible // hidden using alpha (maintain size as placeholder)
    }
    
    private let imageView = UIImageView()
    private let activityIndicator = UIActivityIndicatorView(style: .medium)
    
    private var requestCancellable: AnyCancellable?

    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .clear
        
        fill(with: imageView)
        
        // add activity indicator in the middle
        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    }
    
    func setState(_ state: State) {
        let animations = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            switch state {
            case .loading:
                strongSelf.imageView.alpha = 0
                strongSelf.activityIndicator.isHidden = false
                strongSelf.activityIndicator.startAnimating()
                strongSelf.isHidden = false
                strongSelf.alpha = 1
                
            case let .loaded(image, color):
                strongSelf.imageView.alpha = 1
                strongSelf.imageView.image = image
                if let color = color {
                    // if color is nil, then we assume not color modifications should be made to the image
                    strongSelf.imageView.setImageColor(color)
                }
                strongSelf.activityIndicator.isHidden = true
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.isHidden = false
                strongSelf.alpha = 1
                
            case .hidden:
                strongSelf.isHidden = true
                
            case .invisible:
                strongSelf.alpha = 0
                strongSelf.isHidden = false
            }
            
            strongSelf.setNeedsLayout()
            strongSelf.layoutIfNeeded()
        }
        
        UIView.transition(with: self,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: animations,
                          completion: nil)
    }
}

