//
//  GithubUserView.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

protocol GithubUserViewInput {
    var avatarImageState: CurrentValueSubject<DownloadingImageView.State, Never> { get }
    var isSelected: CurrentValueSubject<Bool, Never> { get }
    var username: String? { get }
}

protocol GithubUserViewOutput {
    func userDidTapUser()
}

class GithubUserView: UIView {
    
    @IBOutlet private var rootView: UIView!
    @IBOutlet private var avatarImageView: DownloadingImageView!
    @IBOutlet private var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet private var nameLabel: UILabel!
    
    private static let stackViewSpacing: CGFloat = 8
    private static let nameLabelFont: UIFont = .app(.caption)
    private static let selectedBorderWidth: CGFloat = 2
    
    private var viewModel: (GithubUserViewInput & GithubUserViewOutput)?
    
    private var viewModelCancelBag = CancellableBag()

    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("GithubUserView", owner: self, options: nil)
        fill(with: rootView)
        
        backgroundColor = .clear
        rootView.backgroundColor = .clear
        
        // configure avatar image view
        imageHeightConstraint.constant = Dimensions.avatarSize.height
        imageWidthConstraint.constant = Dimensions.avatarSize.width
        avatarImageView.layer.cornerRadius = Dimensions.avatarCornerRadius
        avatarImageView.clipsToBounds = true
        
        // configure label
        nameLabel.textColor = .app(.textPrimaryDark)
        nameLabel.font = GithubUserView.nameLabelFont
        nameLabel.textAlignment = .center
    }
    
    static func width(username: String?) -> CGFloat {
        let imageWidth = Dimensions.avatarSize.width
        let textWidth = username?.width(withConstrainedHeight: .greatestFiniteMagnitude, font: GithubUserView.nameLabelFont) ?? 0
        
        return max(imageWidth, textWidth)
    }

    static func desiredHeight() -> CGFloat {
        // calculate the desired height.
        var desiredHeight: CGFloat = 0

        desiredHeight += Dimensions.avatarSize.height
        desiredHeight += GithubUserView.stackViewSpacing
        desiredHeight += "Mock String".height(withConstrainedWidth: .greatestFiniteMagnitude,
                                              font: GithubUserView.nameLabelFont)

        return desiredHeight
    }
    
    func bind(to viewModel: GithubUserViewInput & GithubUserViewOutput) {
        self.viewModel = viewModel
        
        viewModelCancelBag.cancel()
        
        viewModelCancelBag << viewModel.avatarImageState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] avatarImageState in
                self?.avatarImageView.setState(avatarImageState)
        }
        
        viewModelCancelBag << viewModel.isSelected
            .receive(on: DispatchQueue.main)
            .sink { [weak self] isSelected in
                self?.setSelected(isSelected)
        }
        
        nameLabel.text = viewModel.username // don't hide - leave as placeholder
        
        viewModelCancelBag << tapGesturePublisher.sink { [weak self] _ in
            self?.viewModel?.userDidTapUser()
        }
    }
    
    private func setSelected(_ selected: Bool) {
        let animations = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            if selected {
                strongSelf.avatarImageView.layer.borderColor = UIColor.app(.accentColorForLightBackground).cgColor
                strongSelf.avatarImageView.layer.borderWidth = GithubUserView.selectedBorderWidth
            } else {
                strongSelf.avatarImageView.layer.borderColor = nil
                strongSelf.avatarImageView.layer.borderWidth = 0
            }
            
            strongSelf.setNeedsLayout()
            strongSelf.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: 0.2, animations: animations)
    }
}
