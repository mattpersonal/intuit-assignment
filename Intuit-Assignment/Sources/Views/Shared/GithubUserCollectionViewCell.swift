//
//  GithubUserCollectionViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

class GithubUserCollectionViewCell: UICollectionViewCell {
    
    private let userView = GithubUserView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.fill(with: userView)
    }
    
    func bind(to viewModel: GithubUserViewModel) {
        userView.bind(to: viewModel)
    }
    
    static func width(username: String?) -> CGFloat {
        return GithubUserView.width(username: username)
    }
    
    static func desiredHeight() -> CGFloat {
        return GithubUserView.desiredHeight()
    }
}

