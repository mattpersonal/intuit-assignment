//
//  SimpleMessageTableViewCell.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

class SimpleMessageTableViewCell: UITableViewCell {
    
    private let label = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        contentView.fill(with: label, margin: 16)
        
        label.textAlignment = .center
        label.textColor = .app(.textPrimaryDark)
        label.font = .app(.listItemBody)
    }
    
    func setMessage(_ message: String?) {
        label.text = message
    }
}
