//
//  GridLayout.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

protocol GridLayoutDelegate: class {
    func gridLayout(_ gridLayout: GridLayout, heightForItemsInSection section: Int) -> CGFloat
    func gridLayout(_ gridLayout: GridLayout, columnsInSection section: Int) -> Int
    func itemSpacing(gridLayout: GridLayout) -> (verticalSpacing: CGFloat, horizontalSpacing: CGFloat)
}

class GridLayout: UICollectionViewLayout {
    
    weak var gridLayoutDelegate: GridLayoutDelegate?
    
    private var attributesCache = [[UICollectionViewLayoutAttributes]]()
    
    private var contentHeight: CGFloat = 0
    
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        
        let insets = collectionView.contentInset
        return collectionView.bounds.width - insets.left - insets.right
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        guard attributesCache.isEmpty,
            let collectionView = collectionView,
            let delegate = gridLayoutDelegate else {
            return
        }
        
        // grab some values for convenience
        let leftInset = collectionView.contentInset.left
        let rightInset = collectionView.contentInset.right
        let (verticalSpacing, horizontalSpacing) = delegate.itemSpacing(gridLayout: self)
        
        for section in 0 ..< collectionView.numberOfSections {
            var verticalOffset: CGFloat = 0 // track the next vertical position
            if let lastAttribute = attributesCache.last?.last {
                verticalOffset = lastAttribute.frame.maxY + verticalSpacing
            }
            
            var horizontalOffset: CGFloat = 0 // track the next horizontal position
            let columns = max(delegate.gridLayout(self, columnsInSection: section), 1)
            let itemHeight = delegate.gridLayout(self, heightForItemsInSection: section)
            
            // calculating item width
            let totalHorizontalSpacing = horizontalSpacing * CGFloat(columns - 1)
            let itemWidth = (collectionView.bounds.width - leftInset - rightInset - totalHorizontalSpacing) / CGFloat(columns)
            
            var currentColumn = 0
            
            var sectionAttributes = [UICollectionViewLayoutAttributes]()
            for item in 0 ..< collectionView.numberOfItems(inSection: section) {
                let itemFrame = CGRect(x: horizontalOffset,
                                       y: verticalOffset,
                                       width: itemWidth,
                                       height: itemHeight)
                
                // cache the calculate frame
                let attributes = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: item, section: section))
                attributes.frame = itemFrame
                sectionAttributes.append(attributes)
                
                if currentColumn + 1 >= columns {
                    // moving down a row and back to the first column
                    currentColumn = 0
                    horizontalOffset = 0
                    verticalOffset += itemHeight + verticalSpacing
                    
                } else {
                    // stay on same row and move to next column
                    currentColumn += 1
                    horizontalOffset += itemWidth + horizontalSpacing
                }
                
                // update the content height
                contentHeight = max(contentHeight, itemFrame.maxY)
            }
            
            // add the section attributes
            attributesCache.append(sectionAttributes)
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
        
        for sectionAttributes in attributesCache {
            for attributes in sectionAttributes {
                if attributes.frame.intersects(rect) {
                    visibleLayoutAttributes.append(attributes)
                }
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributesCache[indexPath.section][indexPath.item]
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        guard let collectionView = collectionView else {
            return false
        }
        
        // only invalidate if size change - not on scroll
        return collectionView.bounds.size != newBounds.size
    }
    
    override func invalidateLayout() {
        attributesCache.removeAll()
        super.invalidateLayout()
    }
}
