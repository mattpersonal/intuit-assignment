//
//  Strings.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension String {
    
    /// Returns an App String of the given type
    /// - Parameter appString: app string to return the value for
    static func app(_ appString: AppString) -> String {
        return appString.value
    }
}

// MARK: - App String

enum AppString {
    case appName
    case appSubtitle
    
    // MARK: - Repository List
    
    case repoListTitle
    case repoListOwner
    case repoListPublicAccess
    case repoListPrivateAccess
    case repoListEmptyData
    case repoListErrorLoadingData
    
    // MARK: - Repository Detail
    
    case repoDetailTitle
    case repoDetailStars
    case repoDetailWatchers
    case repoDetailForks
    case repoDetailIssues
    case repoDetailContributors
    case repoDetailOwner(ownerName: String)
    
    // MARK: - Issue List
    
    case issueListTitle
    case issueListFilters
    case issueListOpen
    case issueListClosed
    case issueListItemSubtitle(number: Int?, date: Date?, userName: String?)
    case issueListFiltersIssueStatus
    case issueListFiltersUsers
    case issueListFiltersShowAllUsers
    case issueListEmptyData
    case issueListFilterCreatedByUserTitle
    
    // MARK: - Issue Detail
    
    case issueDetailTitle
    case issueDetailNumber(number: Int)
    case issueDetailOpen
    case issueDetailClosed
    case issueDetailClosedOn(date: Date)
    case issueDetailOpenBy(username: String?, date: Date?)
    case issueDetailAssignees
    case issueDetailUnassigned
    case issueDetailDescription
    case issueDetailCommentsSectionHeader
    case issueDetailNoComments
    case issueDetailLabels
    case issueDetailsLabelsEmptyData
    case issueDetailUnknownStatus
    case issueDetailStatusPrompt
    case issueDetailAssigneesEmptyData
    case issueDetailCommentsLoadingError
}

// MARK: - App String Accessors

fileprivate extension AppString {
    
    /// The value of the string
    var value: String {
        // NOTE: this is where we could be checking a dynamically loaded list of strings. For example, let's say we loaded up a JSON of strings. We could check for the string key. If the key is there, then we use the dynamically loaded string. If the key is not there, we use the local/default value. However, we'll just stick with default for this project.
        return defaultValue
    }
    
    /// Fallback value for the string
    private var defaultValue: String {
        
        // NOTE: pretend that these are all NSLocalizedString(key, comment). I decided not to write up descriptions for each NSLocalizedString, but I recognize that comments are very helpful when translators translate all these strings.
        
        switch self {
        case .appName:
            return "The Repo Depot"
            
        case .appSubtitle:
            return "A poor man's Github."
            
        case .repoListTitle:
            return "Repositories"
            
        case .repoListOwner:
            return "Owner"
            
        case .repoDetailTitle:
            return "Repository Details"
            
        case .repoDetailStars:
            return "Stars"
            
        case .repoDetailWatchers:
            return "Watchers"
            
        case .repoDetailForks:
            return "Forks"
            
        case .repoDetailIssues:
            return "Issues"
            
        case .repoDetailContributors:
            return "Contributors"
            
        case .issueListTitle:
            return "Issues"
            
        case .issueListFilters:
            return "Filters"
            
        case .issueListOpen:
            return "Open"
            
        case .issueListClosed:
            return "Closed"
            
        case let .issueListItemSubtitle(number, date, userName):
            // TODO: I would need variations for each combination of number, date, username based on nil and what not. However, that's annoying and I don't think it's necessary for this sample. Therfore, I'll just have one string that really just assumes all are present.
            let numberString: String
            if let number = number {
                numberString = String(number)
            } else {
                numberString = "??"
            }
            
            let dateString = date?.toString(format: .full) ?? "a special day in history"
            let name = userName ?? "someone special"
            
            return "#\(numberString) opened on \(dateString) by \(name)"
            
        case .issueListFiltersIssueStatus:
            return "Issue Status"
            
        case .issueListFiltersUsers:
            return "Users"
            
        case .issueListFiltersShowAllUsers:
            return "Show All"
            
        case .issueDetailTitle:
            return "Issue Details"
            
        case let .issueDetailNumber(number):
            return "#\(number)"
            
        case .issueDetailOpen:
            return "Open"
            
        case .issueDetailClosed:
            return "Closed"
            
        case let .issueDetailClosedOn(date):
            return "on \(date.toString(format: .full))"
            
        case let .issueDetailOpenBy(username, date):
            if let username = username, let date = date {
                return "\(username) opened this issue on \(date.toString(format: .full))"

            } else if let date = date {
                return "Issue opened on \(date.toString(format: .full))"
                
            } else if let username = username {
                return "Opened by \(username)"
                
            } else {
                return ""
            }
            
        case .issueDetailAssignees:
            return "Assignees"
            
        case .issueDetailUnassigned:
            return "Unassigned"
            
        case .issueDetailDescription:
            return "Issue Description"
            
        case .issueDetailCommentsSectionHeader:
            return "Comments"
            
        case .issueDetailNoComments:
            return "No Comments"
            
        case .repoListPublicAccess:
            return "Public"
            
        case .repoListPrivateAccess:
            return "Private"
            
        case .repoListEmptyData:
            return "No repositories."
            
        case let .repoDetailOwner(ownerName):
            return "Owner: \(ownerName)"
            
        case .issueListEmptyData:
            return "No issues."
            
        case .issueDetailLabels:
            return "Labels"
            
        case .issueDetailsLabelsEmptyData:
            return "None"
            
        case .issueDetailUnknownStatus:
            return "unknown"
            
        case .issueDetailStatusPrompt:
            return "Status:"
            
        case .issueDetailAssigneesEmptyData:
            return "None"
            
        case .issueDetailCommentsLoadingError:
            return "Error retrieving comments."
            
        case .issueListFilterCreatedByUserTitle:
            return "Created by:"
            
        case .repoListErrorLoadingData:
            return "Error loading repositories."
        }
    }
}
