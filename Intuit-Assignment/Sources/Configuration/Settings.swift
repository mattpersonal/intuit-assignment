//
//  Settings.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

enum Settings {
    static let mockNetworkCallsLaunchArgument = "Intuit-Assignment.Settings.mockNetworkCallsLaunchArgument"
    static let repositoriesUrlString = "https://api.github.com/users/intuit/repos"
    static let issueNumberMnemonic = "{/number}"
}
