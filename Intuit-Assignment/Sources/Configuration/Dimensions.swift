//
//  Dimensions.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

enum Dimensions {
    
    // MARK: - General
    
    static let cardCornerRadius: CGFloat = 4
    static let standardPadding: CGFloat = 16
    static let tableViewDefaultInsets = UIEdgeInsets(top: 0,
                                                     left: 0,
                                                     bottom: 16,
                                                     right: 0)
    
    static let collectionViewDefaultInsets = UIEdgeInsets(top: 16,
                                                          left: 16,
                                                          bottom: 16,
                                                          right: 16)
    
    // MARK: - Icons + Images
    
    static let smallIconSize = CGSize(width: 16, height: 16)
    static let mediumIconSize = CGSize(width: 32, height: 32)
    static let largeIconSize = CGSize(width: 48, height: 48)
    static let avatarSize = CGSize(width: 36, height: 36)
    static let avatarCornerRadius: CGFloat = 4
}
