//
//  Colors.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// Returns the UIColor for the App Color
    /// - Parameter appColor: app color
    static func app(_ appColor: AppColor) -> UIColor {
        return appColor.color
    }
}

/// The color types we'll use throughout the app
enum AppColor {
    
    // MARK: - Backgrounds
    
    case backgroundDefault
    case backgroundCard
    case backgroundNavigationBar
    case overlayBackground
    case overlayCardBackground
    
    // MARK: - Text Colors
    
    case textPrimaryLight
    case textSecondaryLight
    case textPrimaryDark
    case textSecondaryDark
    
    // MARK: - Accent Colors
    
    case accentColorForLightBackground
    case accentColorForDarkBackground
    case accentPositive
    case accentNegative
    
    // MARK: - Miscellaneous
    
    case shadow
    case neutralIcon
    case separator
}

fileprivate extension AppColor {
    var color: UIColor {
        switch self {
        case .backgroundDefault:
            return Pallette.gray1
            
        case .backgroundCard:
            return .white
            
        case .backgroundNavigationBar:
            return Pallette.navy2
            
        case .textPrimaryLight:
            return .white
            
        case .textSecondaryLight:
            return Pallette.gray3
            
        case .textPrimaryDark:
            return Pallette.gray5
            
        case .textSecondaryDark:
            return Pallette.gray4
            
        case .accentColorForLightBackground:
            return Pallette.orange1
            
        case .accentColorForDarkBackground:
            return Pallette.orange2
            
        case .accentPositive:
            return Pallette.green
            
        case .accentNegative:
            return Pallette.red1
            
        case .shadow:
            return UIColor(white: 0, alpha: 0.3)
            
        case .neutralIcon:
            return Pallette.gray5
            
        case .separator:
            return Pallette.gray2
            
        case .overlayBackground:
            return Pallette.navy1
            
        case .overlayCardBackground:
            return UIColor(white: 1, alpha: 0.1)
        }
    }
}

/// The raw colors we'll use for the app
fileprivate enum Pallette {
    static let gray1 = UIColor(r: 230, g: 231, b: 237)
    static let gray2 = UIColor(r: 200, g: 200, b: 200)
    static let gray3 = UIColor(r: 140, g: 140, b: 140)
    static let gray4 = UIColor(r: 80, g: 80, b: 80)
    static let gray5 = UIColor(r: 40, g: 40, b: 40)
    
    static let navy1 = UIColor(r: 68, g: 69, b: 117)
    static let navy2 = UIColor(r: 33, g: 37, b: 72)
    
    static let orange1 = UIColor(r: 255, g: 157, b: 0)
    static let orange2 = UIColor(r: 255, g: 132, b: 0)
    
    static let red1 = UIColor(r: 255, g: 90, b: 54)
    static let red2 = UIColor(r: 255, g: 64, b: 0)
    
    static let green = UIColor(r: 9, g: 158, b: 47)
}
