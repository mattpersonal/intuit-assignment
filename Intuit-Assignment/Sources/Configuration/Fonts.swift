//
//  Fonts.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

// MARK: - App Fonts

extension UIFont {
    
    /// Returns the request app font
    static func app(_ appFont: AppFont) -> UIFont {
        return appFont.font
    }
}

/// Font types used in the app
enum AppFont {
    
    // MARK: - Navigation Bar
    
    case navigationBarTitle
    
    // MARK: - Headers
    
    case screenHeaderTitlePrimary // an emphasized title
    case screenHeaderTitleSecondary // an less-emphasized title
    case screenHeaderSubtitle
    
    case sectionHeaderTitle
    
    // MARK: - List Items
    
    case listItemTitle
    case listItemTitleLight
    case listItemSubtitle
    case listItemBody
    
    // MARK: - General
    
    case caption
    case body
}

fileprivate extension AppFont {
    var font: UIFont {
        switch self {
        case .navigationBarTitle:
            return .openSans(style: .bold, size: 22)
            
        case .screenHeaderTitlePrimary:
            return .openSans(style: .semibold, size: 22)
            
        case .screenHeaderTitleSecondary:
            return .openSans(style: .light, size: 22)
            
        case .screenHeaderSubtitle:
            return .openSans(style: .regular, size: 14)
            
        case .sectionHeaderTitle:
            return .openSans(style: .semibold, size: 22)
            
        case .listItemTitle:
            return .openSans(style: .semibold, size: 16)
            
        case .listItemTitleLight:
            return .openSans(style: .regular, size: 16)
            
        case .listItemSubtitle:
            return .openSans(style: .regular, size: 12)
            
        case .listItemBody:
            return .openSans(style: .regular, size: 14)
            
        case .caption:
            return .openSans(style: .regular, size: 12)
            
        case .body:
            return .openSans(style: .regular, size: 14)
        }
    }
}

// MARK: - Open Sans

fileprivate extension UIFont {
    
    /// The open sans styles that we'll need
    enum OpenSansStyle {
        case regular
        case semibold
        case bold
        case light
        
        /// The font name that should match the font file. This is used for loading the .ttf files.
        var fontName: String {
            switch self {
            case .regular:
                return "OpenSans"
                
            case .semibold:
                return "OpenSans-Semibold"
                
            case .bold:
                return "OpenSans-Bold"
                
            case .light:
                return "OpenSans-Light"
            }
        }
    }
    
    /// Gets the OpenSans font with the given style and size
    static func openSans(style: OpenSansStyle, size: CGFloat) -> UIFont {
        guard let font = UIFont(name: style.fontName, size: size) else {
            fatalError("Failure to load font with name: \(style.fontName)")
        }
        
        return font
    }
}
