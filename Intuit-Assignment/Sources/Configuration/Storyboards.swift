//
//  Storyboards.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    /// Creates a storyboard using on of the App Storyboards.
    /// - Parameter appStoryboard: app storyboard to create
    static func app(_ appStoryboard: AppStoryboard) -> UIStoryboard {
        return appStoryboard.storyboard
    }
}

// MARK: -  App Storyboards

/// The storyboards used in this app
enum AppStoryboard {
    
    case repositoryList
    case repositoryDetail
    case issueList
    case issueDetail
    
    fileprivate var name: String {
        switch self {
        case .repositoryList:
            return "RepositoryList"
            
        case .repositoryDetail:
            return "RepositoryDetail"
            
        case .issueList:
            return "IssueList"
            
        case .issueDetail:
            return "IssueDetail"
        }
    }
    
    fileprivate var storyboard: UIStoryboard {
        return UIStoryboard(name: name, bundle: nil)
    }
}
