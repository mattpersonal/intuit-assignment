//
//  Images.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UIImage {
    
    static func app(_ appImage: AppImage) -> UIImage? {
        return appImage.image
    }
}

enum AppImage {
    case lock
    case lockOpen
    case eye
    case leftArrow
    case rightArrow
    case document
    case warning
    case twoPeople
    case star
    case tuningFork
    case info
    case textBubble
    case user
}

fileprivate extension AppImage {
    var image: UIImage? {
        switch self {
        case .lock:
            return UIImage(named: "lock.fill")
            
        case .lockOpen:
            return UIImage(named: "lock.open.fill")
            
        case .eye:
            return UIImage(named: "eye.fill")

        case .leftArrow:
            return UIImage(named: "leftArrow")

        case .rightArrow:
            return UIImage(named: "rightArrow")
            
        case .document:
            return UIImage(named: "doc.text.fill")

        case .warning:
            return UIImage(named: "exclamationmark.triangle.fill")

        case .twoPeople:
            return UIImage(named: "person.2.fill")

        case .star:
            return UIImage(named: "star.fill")
            
        case .tuningFork:
            return UIImage(named: "tuningfork")
            
        case .info:
            return UIImage(named: "info.circle.fill")
            
        case .textBubble:
            return UIImage(named: "text.bubble.fill")
            
        case .user:
            return UIImage(named: "person.circle.fill")
        }
    }
}
