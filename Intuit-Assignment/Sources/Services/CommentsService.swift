//
//  CommentsService.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/3/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import RealTalk

protocol CommentsServiceApi {
    
    /// Fetches the comments for an issue in a github repository
    /// - Parameter commentsUrlString: the URL to retrieve the comments from
    /// - Parameter networkingStrategy: strategy (whether or not to mock the data)
    /// - Parameter completion: completion returning the comments or errors encountered
    func fetchRepositories(from commentsUrlString: String?,
                           networkingStrategy: Networker.NetworkingStrategy,
                           completion: @escaping (Result<[Repository.Issue.Comment], Networker.NetworkError>) -> Void) -> URLSessionDataTask?
}

/// Request used for retrieving the list of comments for an issue
struct CommentsNetworkRequest: NetworkRequest {
    typealias ResponseType = [Repository.Issue.Comment]
    
    let urlString: String?
    let httpMethod: NetworkRequestHttpMethod = .get
    
    init(commentsUrlString: String?) {
        urlString = commentsUrlString
    }
}

class CommentsService: CommentsServiceApi {
    
    init() {}
    
    func fetchRepositories(from commentsUrlString: String?,
                           networkingStrategy: Networker.NetworkingStrategy,
                           completion: @escaping (Result<[Repository.Issue.Comment], Networker.NetworkError>) -> Void) -> URLSessionDataTask? {
        let request = CommentsNetworkRequest(commentsUrlString: commentsUrlString)
        
        let strategy: Networker.NetworkingStrategy
        
        if ProcessInfo.processInfo.arguments.contains(Settings.mockNetworkCallsLaunchArgument) {
            // override the strategy using the mocked data (UI testing)
            strategy = .mock(data: .mockedComments, statusCode: 200)
        } else {
            // use the strategy that was passed
            strategy = networkingStrategy
        }
        
        return Networker.shared.perform(request: request,
                                        networkingStrategy: strategy,
                                        completionQueue: .main,
                                        completion: completion)
    }
}
