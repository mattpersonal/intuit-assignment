//
//  IssuesService.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import RealTalk

/// API use to fetch issues from a github repository
protocol IssuesServiceApi {
    
    /// Fetches issues for the github repository
    /// - Parameter issuesUrlString: the URL to retrieve the issues from
    /// - Parameter networkingStrategy: strategy (whether or not to mock the data)
    /// - Parameter completion: completion returning the issues or errors encountered
    func fetchIssues(from issuesUrlString: String?,
                     networkingStrategy: Networker.NetworkingStrategy,
                     completion: @escaping (Result<[Repository.Issue], Networker.NetworkError>) -> Void) -> URLSessionDataTask?
}

/// Request used for retrieving the list of issues for a repository
struct IssuesNetworkRequest: NetworkRequest {
    typealias ResponseType = [Repository.Issue]
    
    let urlString: String?
    let httpMethod: NetworkRequestHttpMethod = .get
    
    init(issuesUrlString: String?) {
        if let issuesUrlString = issuesUrlString {
            // remove the {\number} mnemonic from the url
            urlString = issuesUrlString.replacingOccurrences(of: Settings.issueNumberMnemonic,
                                                             with: "")
        } else {
            urlString = nil
        }
    }
}

class IssuesService: IssuesServiceApi {
    
    init() {}
    
    func fetchIssues(from issuesUrlString: String?,
                     networkingStrategy: Networker.NetworkingStrategy,
                     completion: @escaping (Result<[Repository.Issue], Networker.NetworkError>) -> Void) -> URLSessionDataTask? {
        let request = IssuesNetworkRequest(issuesUrlString: issuesUrlString)
        
        let strategy: Networker.NetworkingStrategy
        
        if ProcessInfo.processInfo.arguments.contains(Settings.mockNetworkCallsLaunchArgument) {
            // override the strategy using the mocked data (UI testing)
            strategy = .mock(data: .mockedIssues, statusCode: 200)
        } else {
            // use the strategy that was passed
            strategy = networkingStrategy
        }
        
        return Networker.shared.perform(request: request,
                                        networkingStrategy: strategy,
                                        completionQueue: .main,
                                        completion: completion)
    }
}
