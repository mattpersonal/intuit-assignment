//
//  RepositoriesService.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine
import RealTalk

/// API use to fetch github repositories
protocol RepositoriesServiceApi {
    
    /// Fetches the github repositories
    /// - Parameter repositoriesUrlString: the URL to retrieve the repositories from
    /// - Parameter networkingStrategy: strategy (whether or not to mock the data)
    /// - Parameter completion: completion returning the repositories or errors encountered
    func fetchRepositories(from repositoriesUrlString: String?,
                           networkingStrategy: Networker.NetworkingStrategy,
                           completion: @escaping (Result<[Repository], Networker.NetworkError>) -> Void) -> URLSessionDataTask?
}

/// Request used for retrieving the list of repositories from github
struct RepositoriesNetworkRequest: NetworkRequest {
    typealias ResponseType = [Repository]
    
    let urlString: String?
    let httpMethod: NetworkRequestHttpMethod = .get
    
    init(repositoriesUrlString: String?) {
        urlString = repositoriesUrlString
    }
}

class RepositoriesService: RepositoriesServiceApi {
    
    init() {}
    
    func fetchRepositories(from repositoriesUrlString: String?,
                                  networkingStrategy: Networker.NetworkingStrategy,
                                  completion: @escaping (Result<[Repository], Networker.NetworkError>) -> Void) -> URLSessionDataTask? {
        let request = RepositoriesNetworkRequest(repositoriesUrlString: repositoriesUrlString)
        
        let strategy: Networker.NetworkingStrategy
        
        if ProcessInfo.processInfo.arguments.contains(Settings.mockNetworkCallsLaunchArgument) {
            // override the strategy using the mocked data (UI testing)
            strategy = .mock(data: .mockedRepositories, statusCode: 200)
        } else {
            // use the strategy that was passed
            strategy = networkingStrategy
        }
        
        return Networker.shared.perform(request: request,
                                        networkingStrategy: strategy,
                                        completionQueue: .main,
                                        completion: completion)
    }
}
