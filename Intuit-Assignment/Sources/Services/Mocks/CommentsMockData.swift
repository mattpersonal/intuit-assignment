//
//  CommentsMockData.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/4/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Data {
    
    static let mockedComments = encodeMock(json: Data.commentsJson)
    
    private static let commentsJson = """
        [
          {
            "url": "https://api.github.com/repos/Mocked-Intuit/automation-for-humans/issues/comments/536072975",
            "html_url": "https://github.com/Mocked-Intuit/automation-for-humans/issues/40#issuecomment-536072975",
            "issue_url": "https://api.github.com/repos/Mocked-Intuit/automation-for-humans/issues/40",
            "id": 536072975,
            "node_id": "MDEyOklzc3VlQ29tbWVudDUzNjA3Mjk3NQ==",
            "user": {
              "login": "trafaliya",
              "id": 19583500,
              "node_id": "MDQ6VXNlcjE5NTgzNTAw",
              "avatar_url": "https://avatars2.githubusercontent.com/u/19583500?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/trafaliya",
              "html_url": "https://github.com/trafaliya",
              "followers_url": "https://api.github.com/users/trafaliya/followers",
              "following_url": "https://api.github.com/users/trafaliya/following{/other_user}",
              "gists_url": "https://api.github.com/users/trafaliya/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/trafaliya/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/trafaliya/subscriptions",
              "organizations_url": "https://api.github.com/users/trafaliya/orgs",
              "repos_url": "https://api.github.com/users/trafaliya/repos",
              "events_url": "https://api.github.com/users/trafaliya/events{/privacy}",
              "received_events_url": "https://api.github.com/users/trafaliya/received_events",
              "type": "User",
              "site_admin": false
            },
            "created_at": "2019-09-27T19:44:53Z",
            "updated_at": "2019-09-27T19:44:53Z",
            "author_association": "NONE",
            "body": "Will create standard NUnit XML reports( based on https://github.com/nunit/docs/wiki/Test-Result-XML-Format). Should be applicable for the framework from the looks of it."
          },
          {
            "url": "https://api.github.com/repos/Mocked-Intuit/automation-for-humans/issues/comments/536082782",
            "html_url": "https://github.com/Mocked-Intuit/automation-for-humans/issues/40#issuecomment-536082782",
            "issue_url": "https://api.github.com/repos/Mocked-Intuit/automation-for-humans/issues/40",
            "id": 536082782,
            "node_id": "MDEyOklzc3VlQ29tbWVudDUzNjA4Mjc4Mg==",
            "user": {
              "login": "MadaraUchiha-314",
              "id": 6977429,
              "node_id": "MDQ6VXNlcjY5Nzc0Mjk=",
              "avatar_url": "https://avatars3.githubusercontent.com/u/6977429?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/MadaraUchiha-314",
              "html_url": "https://github.com/MadaraUchiha-314",
              "followers_url": "https://api.github.com/users/MadaraUchiha-314/followers",
              "following_url": "https://api.github.com/users/MadaraUchiha-314/following{/other_user}",
              "gists_url": "https://api.github.com/users/MadaraUchiha-314/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/MadaraUchiha-314/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/MadaraUchiha-314/subscriptions",
              "organizations_url": "https://api.github.com/users/MadaraUchiha-314/orgs",
              "repos_url": "https://api.github.com/users/MadaraUchiha-314/repos",
              "events_url": "https://api.github.com/users/MadaraUchiha-314/events{/privacy}",
              "received_events_url": "https://api.github.com/users/MadaraUchiha-314/received_events",
              "type": "User",
              "site_admin": false
            },
            "created_at": "2019-09-27T20:18:30Z",
            "updated_at": "2019-09-27T20:18:30Z",
            "author_association": "COLLABORATOR",
            "body": "@trafaliya Would like to look at what are the industry standards for such reports ? Having a comparison between multiple formats would give us better clarity."
          },
          {
            "url": "https://api.github.com/repos/Mocked-Intuit/automation-for-humans/issues/comments/537856640",
            "html_url": "https://github.com/Mocked-Intuit/automation-for-humans/issues/40#issuecomment-537856640",
            "issue_url": "https://api.github.com/repos/Mocked-Intuit/automation-for-humans/issues/40",
            "id": 537856640,
            "node_id": "MDEyOklzc3VlQ29tbWVudDUzNzg1NjY0MA==",
            "user": {
              "login": "trafaliya",
              "id": 19583500,
              "node_id": "MDQ6VXNlcjE5NTgzNTAw",
              "avatar_url": "https://avatars2.githubusercontent.com/u/19583500?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/trafaliya",
              "html_url": "https://github.com/trafaliya",
              "followers_url": "https://api.github.com/users/trafaliya/followers",
              "following_url": "https://api.github.com/users/trafaliya/following{/other_user}",
              "gists_url": "https://api.github.com/users/trafaliya/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/trafaliya/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/trafaliya/subscriptions",
              "organizations_url": "https://api.github.com/users/trafaliya/orgs",
              "repos_url": "https://api.github.com/users/trafaliya/repos",
              "events_url": "https://api.github.com/users/trafaliya/events{/privacy}",
              "received_events_url": "https://api.github.com/users/trafaliya/received_events",
              "type": "User",
              "site_admin": false
            },
            "created_at": "2019-10-03T09:02:58Z",
            "updated_at": "2019-10-03T09:02:58Z",
            "author_association": "NONE",
            "body": "JUnit - https://stackoverflow.com/questions/4922867/what-is-the-junit-xml-format-specification-that-hudson-supports\r\nXUnit - http://reflex.gforge.inria.fr/xunit.html#xunitReport\r\n\r\nThese are some more standard XML reports that are used, generally for unit tests. All are pretty similar, we can choose any one of these."
          }
        ]
    """
}
