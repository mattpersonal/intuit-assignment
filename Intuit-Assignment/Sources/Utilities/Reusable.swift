//
//  Reusable.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

protocol Reusable {
    
    /// Reuse identifier
    static var reuseIdentifier: String { get }
    
}

extension Reusable {
    
    /// By default, just use the class name plus a suffix
    static var reuseIdentifier: String {
        return String(describing: Self.self) + "ReuseIdentifier"
    }
    
}

extension UITableViewCell: Reusable {}
extension UITableViewHeaderFooterView: Reusable {}
extension UICollectionViewCell: Reusable {}
