//
//  Keyboard.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

enum Keyboard {
    
    struct DisplayInfo {
        let isDisplaying: Bool
        let frame: CGRect
    }
    
    /// Publisher producing events whenever the keyboard shows or hides.
    static var displayPublisher: AnyPublisher<DisplayInfo, Never> {
        let willShowPublisher = NotificationCenter.default
            .publisher(for: UIWindow.keyboardWillShowNotification)
            .map { notification -> DisplayInfo in
                guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
                    assertionFailure("Keyboard frame not returned in notification.")
                    return DisplayInfo(isDisplaying: true, frame: .zero)
                }
                
                return DisplayInfo(isDisplaying: true, frame: keyboardFrame.cgRectValue)
        }
        
        let willHidePublisher = NotificationCenter.default
            .publisher(for: UIWindow.keyboardWillHideNotification)
            .map { _ in DisplayInfo(isDisplaying: false, frame: .zero) }
        
        return AnyPublisher(willShowPublisher.merge(with: willHidePublisher))
    }
}
