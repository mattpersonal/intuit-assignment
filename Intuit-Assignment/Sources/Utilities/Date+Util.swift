//
//  Date+Util.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Date {
    
    /// Date formats used when converting dates to strings
    enum Format {
        case fullWithWeekday // Monday, January 1, 2018
        case fullAbbreviatedMonth // Jan 1, 2018
        case full // January 1, 2018
        case fullNumeric // 1/1/2018
        case weekday // Monday
    }
    
    /// Returns a date string in the given format.
    ///
    /// - Parameters:
    ///   - format: date format
    /// - Returns: date string
    func toString(format: Format) -> String {
        let formatter = DateFormatter()
        
        switch format {
        case .fullWithWeekday:
            formatter.dateFormat = "EEEE, MMMM d, yyyy"
            
        case .fullAbbreviatedMonth:
            formatter.dateFormat = "MMM d, yyyy"
            
        case .full:
            formatter.dateFormat = "MMMM d, yyyy"
            
        case .fullNumeric:
            formatter.dateFormat = "MM/dd/yyyy"
            
        case .weekday:
            formatter.dateFormat = "EEEE"
        }
        
        return formatter.string(from: self)
    }
    
}
