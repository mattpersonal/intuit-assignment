//
//  UILabel+Util.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UILabel {
    
    /// Sets the text for the label. Then, if the text is nil or whitespace, the label will be hidden.
    /// - Parameter text: text for the label
    func setTextOrHide(_ text: String?) {
        self.text = text
        self.isHidden = text.isNilOrWhiteSpace
    }
}
