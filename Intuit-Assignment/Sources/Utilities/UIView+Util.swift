//
//  UIView+Util.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Information used to create a shadow on a view
    struct ShadowSpecification {
        let color: UIColor?
        let opacity: Float
        let shadowRadius: CGFloat
        let shadowOffset: CGSize
        
        static var `default`: ShadowSpecification {
            return ShadowSpecification(color: .app(.shadow),
                                       opacity: 1,
                                       shadowRadius: 3,
                                       shadowOffset: .zero)
        }
        
        static var none: ShadowSpecification {
            return ShadowSpecification(color: nil,
                                       opacity: 0,
                                       shadowRadius: 0,
                                       shadowOffset: .zero)
        }
    }
    
    /// Adds a shadow with the given style to the view.
    /// - Parameter style: shadow style
    func setShadow(spec: ShadowSpecification) {
        layer.shadowOpacity = spec.opacity
        layer.shadowColor = spec.color?.cgColor
        layer.shadowRadius = spec.shadowRadius
        layer.shadowOffset = spec.shadowOffset
    }
    
    /// Adds the parameter view to this view with margins using autolayout.
    ///
    /// - Parameters:
    ///   - view: view to add as subview
    ///   - margin: space between subview and this view on all sides
    func fill(with view: UIView?, margin: CGFloat = 0) {
        fill(with: view,
             top: margin,
             trailing: margin,
             bottom: margin,
             leading: margin)
    }
    
    /// Adds the parameter view to this view with margins using autolayout.
    /// - Parameter view: view to add
    /// - Parameter top: top margin
    /// - Parameter trailing: trailing margin
    /// - Parameter bottom: bottom margin
    /// - Parameter leading: leading margin
    func fill(with view: UIView?, top: CGFloat, trailing: CGFloat, bottom: CGFloat, leading: CGFloat) {
        guard let view = view else { return }
        
        // enable autolayout
        view.translatesAutoresizingMaskIntoConstraints = false
        
        // add view
        addSubview(view)
        
        // constrain the view
        view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leading).isActive = true
        view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -trailing).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor, constant: top).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -bottom).isActive = true
    }
}
