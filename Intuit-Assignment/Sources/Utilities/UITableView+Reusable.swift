//
//  UITableView+Reusable.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerReusableClass<ReusableClass>(_ reusableClass: ReusableClass.Type) where ReusableClass: UITableViewCell {
        register(ReusableClass.self, forCellReuseIdentifier: ReusableClass.reuseIdentifier)
    }
    
    func dequeueCellForReusableClass<ReusableClass>(reusableClass: ReusableClass.Type, indexPath: IndexPath) -> ReusableClass where ReusableClass: UITableViewCell {
        return dequeueReusableCell(withIdentifier: ReusableClass.reuseIdentifier, for: indexPath) as! ReusableClass
    }
    
    func dequeueCellForReusableClass<ReusableClass>(reusableClass: ReusableClass.Type) -> ReusableClass? where ReusableClass: UITableViewCell {
        return dequeueReusableCell(withIdentifier: ReusableClass.reuseIdentifier) as? ReusableClass
    }
}
