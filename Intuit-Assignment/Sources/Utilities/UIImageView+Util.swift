//
//  UIImageView+Util.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImageColor(_ color: UIColor?) {
        guard let image = image else { return }
        
        self.image = image.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}

