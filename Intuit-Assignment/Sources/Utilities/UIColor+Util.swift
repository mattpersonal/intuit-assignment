//
//  UIColor+Util.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// Creates a color given the RGB values and the alpha.
    ///
    /// - Parameters:
    ///   - r: red RGB value 0-255
    ///   - g: green RGB value 0-255
    ///   - b: blue RGB value 0-255
    ///   - alpha: alpha 0.0-1.0
    convenience init(r: Int, g: Int, b: Int, alpha: CGFloat = 1) {
        let rgbRange = 0 ... 255
        
        let constrainedR = r.constrain(to: rgbRange)
        let constrainedG = g.constrain(to: rgbRange)
        let constrainedB = b.constrain(to: rgbRange)
        
        self.init(red: CGFloat(constrainedR)/255,
                  green: CGFloat(constrainedG)/255,
                  blue: CGFloat(constrainedB)/255,
                  alpha: alpha)
    }
    
    /// Creates a UIColor given the hex string.
    /// - Parameter hexString: hex string representing the color. The hex string is expected to be formatted with just the RGB values specified in hex form. The hex string can be prefixed with '#'.
    /// - Parameter alpha: the alpha from 0 to 1 for the color
    convenience init?(hexString: String, alpha: CGFloat = 1.0) {
        // solution adapted from the following post:
        // https://medium.com/@zumrywahid/swift-4-hex-color-to-uicolor-c7649ad5bfd2
        
        // prepare the string and perform validation checks
        let trimmedHexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let finalHexString: String
        if trimmedHexString.hasPrefix("#") {
            finalHexString = String(trimmedHexString.dropFirst())
        } else {
            finalHexString = trimmedHexString
        }
        
        // A hex RGB string should be 6 characters (RRGGBB)
        guard finalHexString.count == 6 else {
            return nil
        }
                
        // This mask will be used to isolate the R, G, and B portions of the RGB hex string.
        let mask = 0x000000FF
        
        let scanner = Scanner(string: finalHexString)
        var color: UInt64 = 0
        scanner.scanHexInt64(&color)
        
        // R: bits 16-23
        // G: bits 8-15
        // B: bits 0-7
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        self.init(r: r, g: g, b: b, alpha: alpha)
    }
    
    /// Gets the RGB value and alpha from the UIColor.
    /// - Returns: red (0-255), green (0-255), blue (0-255), alpha (0-1)
    func rgb() -> (red:Int, green:Int, blue:Int, alpha:Int)? {
        // solution adapted from this stack overflow post:
        // https://stackoverflow.com/a/32846745
        
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha)
            
            return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}
