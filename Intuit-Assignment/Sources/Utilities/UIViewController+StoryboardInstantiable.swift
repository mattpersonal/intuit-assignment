//
//  UIViewController+StoryboardInstantiable.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

/// Protocol for view controllers created in storyboards
protocol StoryboardInstantiable: UIViewController {
    
    /// Identifier in the storyboard
    static var storyboardIdentifier: String { get }
    
    /// Storyboard used to instantiate the view controller
    static var storyboard: UIStoryboard { get }
    
    /// Instantiates the UIViewController from the storyboard
    static func instantiate() -> Self
    
}

extension StoryboardInstantiable {
    static var storyboardIdentifier: String {
        // by default, use the class name of the view controller
        return String(describing: Self.self)
    }
    
    static func instantiate() -> Self {
        return storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! Self
    }
}
