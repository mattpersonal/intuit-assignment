//
//  ImageDownloader.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine

enum ImageDownloader {
    
    /// Downloads an image given the URL
    /// - Parameter urlString: url string used to download the image
    /// - Parameter completion: completion returns the downloaded image
    static func download(urlString: String, completion: @escaping (UIImage?) -> Void) -> AnyCancellable? {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return nil
        }
        return URLSession.shared.dataTaskPublisher(for: url)
            .sink(receiveCompletion: { taskCompletion in
                switch taskCompletion {
                case .finished:
                    // do nothing
                    break
                    
                case .failure:
                    completion(nil)
                }
            }) { taskResult in
                let (data, _) = taskResult
                completion(UIImage(data: data))
        }
    }
}
