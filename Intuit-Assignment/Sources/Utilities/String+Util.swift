//
//  String+Util.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit

extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    /// Returns the formatted string using this string's value and the provided parameters
    /// - Parameter parameters: parameters to include in the formatted string
    func parameterized(_ parameters: String...) -> String {
        return String(format: self, arguments: parameters)
    }
    
    /// Convenience function for creating an attributed string
    /// - Parameter textColor: text color
    /// - Parameter font: font
    func attributed(textColor: UIColor? = nil,
                    font: UIFont? = nil) -> NSAttributedString {
        var attributes = [NSAttributedString.Key : Any]()
        
        if let textColor = textColor {
            attributes[.foregroundColor] = textColor
        }
        
        if let font = font {
            attributes[.font] = font
        }
        
        return NSAttributedString(string: self,
                                  attributes: attributes)
    }
    
    /// Returns nil if the string is nil or whitespace. Otherwise, the string is returned
    var unwrap: String? {
        // if whitespace, return nil even though the string variable is not nil.
        if isNilOrWhiteSpace {
            return nil
        }
        
        return self
    }
    
    /// Determines if the string is null whitespace (the string cannot be null, but leaving this named as "isNullOrWhiteSpace" to be consistent with the "isNullOrWhiteSpace" function in the Optional extension
    var isNilOrWhiteSpace: Bool {
        // check for empty string
        if isEmpty {
            return true
        }
        
        // check for only whitespace
        let trimmedString = trimmingCharacters(in: .whitespacesAndNewlines)
        if trimmedString.isEmpty {
            return true
        }
        
        // string must have non-whitespace characters
        return false
    }
}

extension Optional where Wrapped == String {
    
    /// Determines if the string is either nil or whitespace
    var isNilOrWhiteSpace: Bool {
        // check for nil
        guard let string = self else {
            return true
        }
        
        return string.isNilOrWhiteSpace
    }
}
