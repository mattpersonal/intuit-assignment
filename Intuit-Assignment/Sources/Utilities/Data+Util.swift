//
//  Data+Util.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/4/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Data {
    
    static func encodeMock(json: String) -> Data {
        let cleanedJson = json.replacingOccurrences(of: "\r\n", with: "")
        return cleanedJson.data(using: .utf8)!
    }
}
