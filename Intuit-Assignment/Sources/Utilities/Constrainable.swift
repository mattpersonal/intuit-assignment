//
//  Constrainable.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

protocol Constrainable {}

/// Constrainable should be adopted by anything that can be constrained between two values. 
extension Constrainable where Self: Comparable {
    
    /// Constrains the value to the range.
    /// - Parameter range: range of allowed values
    func constrain(to range: ClosedRange<Self>) -> Self {
        let minimum = min(self, range.upperBound)
        return max(self, minimum)
    }
}

extension Int: Constrainable {}
extension Double: Constrainable {}
extension Float: Constrainable {}
