//
//  IssueDetailViewController.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import ReactiveExtras
import Combine

protocol IssueDetailViewInput {
    var screenTitle: String? { get }
    var repositoryName: String? { get }
    var sections: [IssueDetailSection] { get }
}

protocol IssueDetailViewOutput {
    
}

/// The high-level sections that will display on the Issue Details page
enum IssueDetailSection {
    case summary(viewModel: IssueDetailSummarySectionViewModel)
    case comments(viewModel: IssueDetailCommentSectionViewModel)
}

/// The individual items that display in the summary section of the Issue Details page
enum IssueDetailSummarySectionItem {
    case issueName(viewModel: IssueDetailNameViewModel)
    case labels(viewModel: IssueDetailLabelsViewModel)
    case status(viewModel: IssueDetailStatusViewModel)
    case assignees(viewModel: IssueDetailAssigneesViewModel)
    case description(viewModel: IssueDetailDescriptionViewModel  )
}

/// Type of table rows that can display in the comments section of the Issue Detail page
enum IssueDetailCommentRowType {
    case loading // loading cell
    case simpleMessage(String?) // like "empty data" or "error loading"
    case issueComment(IssueDetailCommentViewModel) // a comment made my a github user
}

class IssueDetailViewController: UIViewController {
    
    @IBOutlet private var repositoryNameContainerView: UIView!
    @IBOutlet private var repositoryNameLabel: UILabel!
    @IBOutlet private var tableView: UITableView!
    
    private var viewModel: (IssueDetailViewInput & IssueDetailViewOutput)?
    
    private var hasUserScrolledSinceLastDataLoad = false
    
    private var viewModelCancelBag = CancellableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        styleUI()
        setupTableView()
        
        if let viewModel = viewModel {
            bind(to: viewModel)
        }
    }
    
    private func styleUI() {
        view.backgroundColor = .app(.backgroundDefault)
        tableView.backgroundColor = .clear
        repositoryNameContainerView.backgroundColor = .app(.backgroundNavigationBar)
        repositoryNameLabel.textColor = .app(.textPrimaryLight)
        repositoryNameLabel.font = .app(.screenHeaderSubtitle)
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = .clear
        tableView.contentInset = Dimensions.tableViewDefaultInsets
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        
        tableView.registerReusableClass(IssueDetailLabelsTableViewCell.self)
        tableView.registerReusableClass(IssueDetailNameTableViewCell.self)
        tableView.registerReusableClass(IssueDetailStatusTableViewCell.self)
        tableView.registerReusableClass(IssueDetailDescriptionTableViewCell.self)
        tableView.registerReusableClass(IssueDetailAssigneesTableViewCell.self)
        tableView.registerReusableClass(IssueDetailCommentTableViewCell.self)
        tableView.registerReusableClass(SimpleMessageTableViewCell.self)
        tableView.registerReusableClass(LoadingTableViewCell.self)

        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func bind(to viewModel: IssueDetailViewInput & IssueDetailViewOutput) {
        self.viewModel = viewModel
        
        guard isViewLoaded else {
            return
        }
        
        viewModelCancelBag.cancel()
        
        title = viewModel.screenTitle
        repositoryNameLabel.setTextOrHide(viewModel.repositoryName)
        
        // reload the data since we have a new view model
        tableView.reloadData()
        hasUserScrolledSinceLastDataLoad = false
        
        for (sectionIndex, section) in viewModel.sections.enumerated() {
            switch section {
            case .summary:
                break // nothing needed
                
            case let .comments(viewModel):
                viewModelCancelBag << viewModel.commentViewModels
                    .receive(on: DispatchQueue.main)
                    .sink(receiveValue: { [weak self] arrayOperation in
                        self?.tableView.perform(operation: arrayOperation, section: sectionIndex)
                    })
            }
        }
    }
    
    private func makeNameCell(viewModel: IssueDetailNameViewModel,
                              indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCellForReusableClass(reusableClass: IssueDetailNameTableViewCell.self, indexPath: indexPath)
        cell.bind(to: viewModel)
        return cell
    }
    
    private func makeLabelsItemCell(viewModel: IssueDetailLabelsViewModel,
                                    indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCellForReusableClass(reusableClass: IssueDetailLabelsTableViewCell.self, indexPath: indexPath)
        cell.bind(to: viewModel)
        return cell
    }
    
    private func makeStatusItemCell(viewModel: IssueDetailStatusViewModel,
                                    indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCellForReusableClass(reusableClass: IssueDetailStatusTableViewCell.self, indexPath: indexPath)
        cell.bind(to: viewModel)
        return cell
    }
    
    private func makeDescriptionItemCell(viewModel: IssueDetailDescriptionViewModel,
                                         indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCellForReusableClass(reusableClass: IssueDetailDescriptionTableViewCell.self, indexPath: indexPath)
        cell.bind(to: viewModel)
        return cell
    }
    
    private func makeAssigneesItemCell(viewModel: IssueDetailAssigneesViewModel,
                                       indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCellForReusableClass(reusableClass: IssueDetailAssigneesTableViewCell.self, indexPath: indexPath)
        cell.bind(to: viewModel)
        return cell
    }
    
    private func makeCommentItemCell(viewModel: IssueDetailCommentViewModel,
                                     indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCellForReusableClass(reusableClass: IssueDetailCommentTableViewCell.self, indexPath: indexPath)
        cell.bind(to: viewModel)
        return cell
    }
    
    
    private func makeSimpleMessageItemCell(message: String?,
                                           indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCellForReusableClass(reusableClass: SimpleMessageTableViewCell.self, indexPath: indexPath)
        cell.setMessage(message)
        return cell
    }
    
    private func makeLoadingItemCell(indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueCellForReusableClass(reusableClass: LoadingTableViewCell.self, indexPath: indexPath)
    }
}

extension IssueDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.sections.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        
        switch viewModel.sections[section] {
        case let .summary(viewModel):
            return viewModel.items.count
            
        case let .comments(viewModel):
            return viewModel.commentViewModels.elementCount
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            return UITableViewCell()
        }
        
        switch viewModel.sections[indexPath.section] {
        case let .summary(viewModel):
            switch viewModel.items[indexPath.row] {
            case let .issueName(viewModel):
                return makeNameCell(viewModel: viewModel, indexPath: indexPath)
                
            case let .labels(viewModel):
                return makeLabelsItemCell(viewModel: viewModel, indexPath: indexPath)
                
            case let .assignees(viewModel):
                return makeAssigneesItemCell(viewModel: viewModel, indexPath: indexPath)
                
            case let .description(viewModel):
                return makeDescriptionItemCell(viewModel: viewModel, indexPath: indexPath)
                
            case let .status(viewModel):
                return makeStatusItemCell(viewModel: viewModel, indexPath: indexPath)
            }
            
        case let .comments(commentsSectionViewModel):
            switch commentsSectionViewModel.commentViewModels[indexPath.row] {
            case let .issueComment(viewModel):
                return makeCommentItemCell(viewModel: viewModel,
                                           indexPath: indexPath)
                
            case let .simpleMessage(message):
                return makeSimpleMessageItemCell(message: message, indexPath: indexPath)
                
            case .loading:
                return makeLoadingItemCell(indexPath: indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let viewModel = viewModel else {
            return nil
        }
        
        switch viewModel.sections[section] {
        case .summary:
            return nil
            
        case let .comments(viewModel):
            return viewModel.header
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard hasUserScrolledSinceLastDataLoad == false else {
            // if the user has already scrolled, don't animate the cells in. This is my way of animating cells in only on initial load.
            return
        }
        
        let delayOffset: CGFloat = 0.05
        var delay: CGFloat = 0
        if indexPath.section > 0 {
            for section in 0 ..< indexPath.section {
                delay += delayOffset * CGFloat(tableView.numberOfRows(inSection: section))
            }
        }
        delay += CGFloat(indexPath.row) * delayOffset
        delay = min(delay, 1)
        
        cell.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.5,
                       delay: TimeInterval(delay),
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.1,
                       options: .curveLinear,
                       animations: {
                        cell.transform = .identity
        },
                       completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hasUserScrolledSinceLastDataLoad = true
    }
}

extension IssueDetailViewController: StoryboardInstantiable {
    static var storyboard: UIStoryboard {
        return .app(.issueDetail)
    }
}
