//
//  IssueListViewController.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

protocol IssueListViewInput {
    var screenTitle: String? { get }
    var repositoryName: String? { get }
    
    /// Use published array since the displayed rows will change based on the current filters
    var displayedIssueViewModels: PublishedArray<IssueItemViewModel> { get }
    
    var viewState: CurrentValueSubject<IssueListViewState, Never> { get }
    var showIssueDetailsPublisher: AnyPublisher<(repository: Repository, issue: Repository.Issue), Never> { get }
}

protocol IssueListViewOutput {
    func loadData()
}

enum IssueListViewState {
    case loading
    case error(String?)
    case displayingData
    case emptyData(String?)
}

class IssueListViewController: UIViewController {
    
    @IBOutlet private var headerBackgroundView: UIView!
    @IBOutlet private var repositoryNameLabel: UILabel!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var loadingIndicator: UIActivityIndicatorView!
    
    private var viewModel: (IssueListViewInput & IssueListViewOutput)?
    
    private var viewModelCancelBag = CancellableBag() // cancelled when new view model is set
    
    // this keeps track of if the user has scrolled so that we can stop animating the cells in
    private var hasUserScrolledAfterTableReload = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        styleUI()
        setupTableView()
        
        if let viewModel = viewModel {
            bind(to: viewModel)
        }
    }
    
    private func styleUI() {
        view.backgroundColor = .app(.backgroundDefault)
        headerBackgroundView.backgroundColor = .app(.backgroundNavigationBar)
        
        repositoryNameLabel.textColor = .app(.textPrimaryLight)
        repositoryNameLabel.font = .app(.screenHeaderSubtitle)
        errorLabel.textColor = .app(.textPrimaryDark)
        errorLabel.font = .app(.body)
        errorLabel.text = nil
        
        tableView.accessibilityIdentifier = "IssueListViewController.tableView"
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = .clear
        tableView.contentInset = Dimensions.tableViewDefaultInsets
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        
        tableView.registerReusableClass(IssueItemTableViewCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func bind(to viewModel: IssueListViewInput & IssueListViewOutput) {
        self.viewModel = viewModel
        
        guard isViewLoaded else {
            return
        }
        
        viewModel.loadData()
        
        viewModelCancelBag.cancel()
        
        title = viewModel.screenTitle
        repositoryNameLabel.setTextOrHide(viewModel.repositoryName)
        
        viewModelCancelBag << viewModel.viewState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                self?.setViewState(state)
        }
        
        // reload the data to start since we have new data
        tableView.reloadData()
        hasUserScrolledAfterTableReload = false
        viewModelCancelBag << viewModel.displayedIssueViewModels
            .receive(on: DispatchQueue.main)
            .sink { [weak self] arrayOperation in
                self?.tableView.perform(operation: arrayOperation, section: 0)
            }
        
        viewModelCancelBag << viewModel.showIssueDetailsPublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (repo, issue) in
                self?.showIssueDetails(repository: repo, issue: issue)
        }
    }
    
    private func setViewState(_ state: IssueListViewState) {
        let animations: () -> Void = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            // TODO: probably a more efficient way of doing this - unimportant though. Lots to do!
            switch state {
            case .displayingData:
                strongSelf.tableView.alpha = 1
                strongSelf.errorLabel.alpha = 0
                strongSelf.loadingIndicator.stopAnimating()
                strongSelf.loadingIndicator.alpha = 0
                
            case .loading:
                strongSelf.tableView.alpha = 0
                strongSelf.errorLabel.alpha = 0
                strongSelf.loadingIndicator.startAnimating()
                strongSelf.loadingIndicator.alpha = 1
                
            case let .error(errorDescription):
                strongSelf.tableView.alpha = 0
                strongSelf.errorLabel.alpha = 1
                strongSelf.errorLabel.text = errorDescription
                strongSelf.loadingIndicator.stopAnimating()
                strongSelf.loadingIndicator.alpha = 0
                
            case let .emptyData(displayString):
                strongSelf.tableView.alpha = 0
                strongSelf.errorLabel.alpha = 1
                strongSelf.errorLabel.text = displayString
                strongSelf.loadingIndicator.stopAnimating()
                strongSelf.loadingIndicator.alpha = 0
            }
            
            strongSelf.view.setNeedsLayout()
            strongSelf.view.layoutIfNeeded()
        }
        
        UIView.transition(with: view,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: animations,
                          completion: nil)
    }
    
    private func showIssueDetails(repository: Repository, issue: Repository.Issue) {
        let viewController = IssueDetailViewController.instantiate()
        let issueViewModel = IssueDetailViewModel(repository: repository, issue: issue)
        viewController.bind(to: issueViewModel)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension IssueListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.displayedIssueViewModels.elementCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueCellForReusableClass(reusableClass: IssueItemTableViewCell.self,
                                                         indexPath: indexPath)
        
        let itemViewModel = viewModel.displayedIssueViewModels[indexPath.row]
        cell.bind(to: itemViewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard hasUserScrolledAfterTableReload == false else {
            // if the user has already scrolled, don't animate the cells in. This is my way of animating cells in only on initial load.
            return
        }
        
        let delayOffset: CGFloat = 0.05
        var delay: CGFloat = 0
        if indexPath.section > 0 {
            for section in 0 ..< indexPath.section {
                delay += delayOffset * CGFloat(tableView.numberOfRows(inSection: section))
            }
        }
        delay += CGFloat(indexPath.row) * delayOffset
        delay = min(delay, 1)
        
        cell.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.5,
                       delay: TimeInterval(delay),
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.1,
                       options: .curveLinear,
                       animations: {
                        cell.transform = .identity
        },
                       completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hasUserScrolledAfterTableReload = true
    }
}

extension IssueListViewController: StoryboardInstantiable {
    static var storyboard: UIStoryboard {
        return .app(.issueList)
    }
}
 
