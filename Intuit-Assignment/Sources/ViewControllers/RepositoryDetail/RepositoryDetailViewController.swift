//
//  RepositoryDetailViewController.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import Combine
import ReactiveExtras

protocol RepositoryDetailViewInput {
    var screenTitle: String? { get }
    var repositoryName: String? { get }
    var ownerNameString: String? { get }
    var repositoryDescription: String? { get }
    var sectionViewModels: [RepositoryDetailSectionViewModel] { get }
    var showIssuesListPublisher: AnyPublisher<Repository, Never> { get }
}

protocol RepositoryDetailViewOutput {
    
}

/// The various ways a particular `RepositoryDetailItem` can be displayed
enum RepositoryDetailItemDisplayStyle {
    case small // small grid item
    case large // full width grid item
}

class RepositoryDetailViewController: UIViewController {
    
    @IBOutlet private var repoNameLabel: UILabel!
    @IBOutlet private var ownerNameLabel: UILabel!
    @IBOutlet private var repoDescriptionLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
    
    private var viewModel: (RepositoryDetailViewInput & RepositoryDetailViewOutput)?
    
    private var viewModelCancelBag = CancellableBag()
    
    /// the index paths that we have already animated in. we track them so we don't animate them twice just when scrolling
    private var animatedIndexPaths = [IndexPath]()

    override func viewDidLoad() {
        super.viewDidLoad()

        styleUI()
        setupCollectionView()
        
        if let viewModel = viewModel {
            bind(to: viewModel)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func styleUI() {
        view.backgroundColor = .app(.backgroundDefault)
        
        repoNameLabel.textColor = .app(.textPrimaryDark)
        repoNameLabel.font = .app(.screenHeaderTitlePrimary)
        ownerNameLabel.textColor = .app(.textSecondaryDark)
        ownerNameLabel.font = .app(.screenHeaderSubtitle)
        repoDescriptionLabel.textColor = .app(.textPrimaryDark)
        repoDescriptionLabel.font = .app(.body)
    }
    
    private func setupCollectionView() {
        collectionView.contentInset = Dimensions.collectionViewDefaultInsets
        collectionView.backgroundColor = .clear
        
        collectionView.register(RepositoryDetailItemSmallCollectionViewCell.self, forCellWithReuseIdentifier: RepositoryDetailItemSmallCollectionViewCell.reuseIdentifier)
        collectionView.register(RepositoryDetailItemLargeCollectionViewCell.self, forCellWithReuseIdentifier: RepositoryDetailItemLargeCollectionViewCell.reuseIdentifier)
        
        let gridLayout = GridLayout()
        gridLayout.gridLayoutDelegate = self
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = gridLayout
    }
    
    func bind(to viewModel: RepositoryDetailViewInput & RepositoryDetailViewOutput) {
        self.viewModel = viewModel

        guard isViewLoaded else {
            return
        }
        
        viewModelCancelBag.cancel()
        
        title = viewModel.screenTitle
        repoNameLabel.setTextOrHide(viewModel.repositoryName)
        ownerNameLabel.setTextOrHide(viewModel.ownerNameString)
        repoDescriptionLabel.setTextOrHide(viewModel.repositoryDescription)
        
        // reload the collection view since we have new data and also clear out any index paths for animation purposes
        animatedIndexPaths.removeAll()
        collectionView.reloadData()
        
        viewModelCancelBag << viewModel.showIssuesListPublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] repository in
                self?.showIssuesList(repository: repository)
        }
    }
    
    private func showIssuesList(repository: Repository) {
        let viewController = IssueListViewController.instantiate()
        let issueListViewModel = IssueListViewModel(repository: repository)
        viewController.bind(to: issueListViewModel)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension RepositoryDetailViewController: GridLayoutDelegate {
    func gridLayout(_ gridLayout: GridLayout, heightForItemsInSection section: Int) -> CGFloat {
        guard let viewModel = viewModel else {
            return 0
        }
        
        let sectionViewModel = viewModel.sectionViewModels[section]
        
        switch sectionViewModel.itemDisplayStyle {
        case .small:
            return 110
            
        case .large:
            return 65
        }
    }
    
    func gridLayout(_ gridLayout: GridLayout, columnsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        
        let sectionViewModel = viewModel.sectionViewModels[section]
        
        switch sectionViewModel.itemDisplayStyle {
        case .small:
            return 2
            
        case .large:
            return 1
        }
    }
    
    func itemSpacing(gridLayout: GridLayout) -> (verticalSpacing: CGFloat, horizontalSpacing: CGFloat) {
        return (16, 16)
    }
}

extension RepositoryDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel?.sectionViewModels.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.sectionViewModels[section].itemViewModels.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var returnCell = UICollectionViewCell()
        
        guard let viewModel = viewModel else {
            return returnCell
        }
        
        let sectionViewModel = viewModel.sectionViewModels[indexPath.section]
        let itemViewModel = sectionViewModel.itemViewModels[indexPath.item]
        
        switch sectionViewModel.itemDisplayStyle {
        case .small:
            guard let smallCell = collectionView.dequeueReusableCell(withReuseIdentifier: RepositoryDetailItemSmallCollectionViewCell.reuseIdentifier,
                                                                     for: indexPath) as? RepositoryDetailItemSmallCollectionViewCell else {
                                                                        return returnCell
            }
            
            smallCell.bind(to: itemViewModel)
            returnCell = smallCell
            
        case .large:
            guard let largeCell = collectionView.dequeueReusableCell(withReuseIdentifier: RepositoryDetailItemLargeCollectionViewCell.reuseIdentifier,
                                                                  for: indexPath) as? RepositoryDetailItemLargeCollectionViewCell else {
                return returnCell
            }
            
            largeCell.bind(to: itemViewModel)
            returnCell = largeCell
        }
        
        return returnCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard animatedIndexPaths.contains(indexPath) == false else {
            return
        }
        
        animatedIndexPaths.append(indexPath)
        
        let delayOffset: CGFloat = 0.05
        var delay: CGFloat = 0
        if indexPath.section > 0 {
            for section in 0 ..< indexPath.section {
                delay += delayOffset * CGFloat(collectionView.numberOfItems(inSection: section))
            }
        }
        delay += CGFloat(indexPath.item) * delayOffset
        delay = min(delay, 1)
        
        cell.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.5,
                       delay: TimeInterval(delay),
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.1,
                       options: .curveLinear,
                       animations: {
                        cell.transform = .identity
            },
                       completion: nil)
    }
}

extension RepositoryDetailViewController: StoryboardInstantiable {
    static var storyboard: UIStoryboard {
        return .app(.repositoryDetail)
    }
}
