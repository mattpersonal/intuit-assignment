//
//  RepositoryListViewController.swift
//  Intuit-Assignment
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import UIKit
import ReactiveExtras
import Combine

/// Input information needed by the RepositoryListViewController
protocol RepositoryListViewInput {
    
    /// Title for the repository list page
    var screenTitle: String? { get }
    
    /// The sections view models for the repository list page. The repository list page is broken down into sections based on owner (`GithubUser`).
    var sectionViewModels: CurrentValueSubject<[RepositoryListSectionInput], Never> { get }
    
    /// The current state of the list page.
    var viewState: CurrentValueSubject<RepositoryListViewState, Never> { get }
    
    /// Publisher producing events whenever the details should be shown for a `Repository`
    var showRepositoryDetailsPublisher: AnyPublisher<Repository, Never> { get }
}

/// Output interface for the RepositoryListViewController
protocol RepositoryListViewOutput {
    
    /// Called when the view is ready for the data to be loaded
    func loadData(completion: (() -> Void)?)
    
    /// called as user types in the repositories search box
    func userUpdatedSearch(_ search: String?)
}

/// Input for each section in the list.
protocol RepositoryListSectionInput {
    
    /// Determines if the section header should be displayed at all
    var shouldDisplayHeader: Bool { get }
    
    /// View model representing the owner of the repositories
    var repositoryOwnerViewModel: RepositoryListOwnerViewModel { get }
    
    /// View model for each repository that will display in this section.
    /// Each view model represents a row in the section.
    var displayedRepositoryViewModels: PublishedArray<RepositoryItemViewModel> { get }
}

/// Possible states the Repository List page can be in at any time
enum RepositoryListViewState: Equatable {
    case loading
    case error(String?)
    case displayingData
    case emptyData(String?)
}

/// View Controller for the Repository List page
class RepositoryListViewController: UIViewController {
    
    @IBOutlet private var searchBarContainerView: UIView!
    @IBOutlet private var searchTextField: UITextField!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var loadingIndicator: UIActivityIndicatorView!
    
    private let viewModel: RepositoryListViewInput & RepositoryListViewOutput = RepositoryListViewModel()
    
    private var lifetimeCancelBag = CancellableBag() // never cancelled
    private var viewModelCancelBag = CancellableBag() // cancelled when new view model is set
    private var sectionViewModelCancelBag = CancellableBag() // cancelled when new section view models are set
    
    // this keeps track of if the user has scrolled so that we can stop animating the cells in
    private var hasUserScrolledAfterTableReload = false

    override func viewDidLoad() {
        super.viewDidLoad()

        styleUI()
        setAccessibilityIdentifiers()
        setupTableView()
        setupKeyboardHandling()
        bindToViewModel()
        viewModel.loadData(completion: nil)
    }
    
    private func styleUI() {
        view.backgroundColor = .app(.backgroundDefault)
        searchBarContainerView.backgroundColor = .app(.backgroundNavigationBar)
        errorLabel.textColor = .app(.textPrimaryDark)
        errorLabel.font = .app(.body)
        errorLabel.text = nil
    }
    
    private func setAccessibilityIdentifiers() {
        // setup identifiers for easier UI testing
        tableView.accessibilityIdentifier = "RepositoryListViewController.tableView"
        searchTextField.accessibilityIdentifier = "RepositoryListViewController.searchTextField"
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = .clear
        tableView.contentInset = Dimensions.tableViewDefaultInsets
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 100
        
        tableView.registerReusableClass(RepositoryItemTableViewCell.self)
        tableView.register(RepositoryListOwnerHeaderView.self,
                           forHeaderFooterViewReuseIdentifier: RepositoryListOwnerHeaderView.reuseIdentifier)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupKeyboardHandling() {
        // dismiss keyboard on background tap
        lifetimeCancelBag << view.tapGesturePublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.view.endEditing(true)
        }
        
        // same for navigation bar (TODO: we could merge this publisher with the view publisher above, but not important)
        if let navigationBar = navigationController?.navigationBar {
            lifetimeCancelBag << navigationBar.tapGesturePublisher
                .receive(on: DispatchQueue.main)
                .sink { [weak self] _ in
                    self?.view.endEditing(true)
                }
        }
        
        // update the table view insets to account for the keyboard
        lifetimeCancelBag << Keyboard.displayPublisher.sink { [weak self] keyboardInfo in
            let keyboardHeight: CGFloat
            if keyboardInfo.isDisplaying {
                keyboardHeight = keyboardInfo.frame.height
            } else {
                keyboardHeight = 0
            }
            
            var defaultInsets = Dimensions.tableViewDefaultInsets
            defaultInsets.bottom += keyboardHeight
            self?.tableView.contentInset = defaultInsets
        }
    }
    
    private func bindToViewModel() {
        viewModelCancelBag.cancel()
        
        title = viewModel.screenTitle
        
        viewModelCancelBag << viewModel.viewState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                self?.setViewState(state)
        }
        
        viewModelCancelBag << viewModel.sectionViewModels
            .receive(on: DispatchQueue.main)
            .sink { [weak self] sectionViewModels in
                self?.updateWithSectionViewModels(sectionViewModels)
        }
        
        viewModelCancelBag << searchTextField.editingPublisher
            .filter { editingEvent -> Bool in
                // we only want events when editing changed meaning the search text changed
                return editingEvent.eventType == .editingChanged
            }
            .debounce(for: 0.5, scheduler: DispatchQueue.main)
            .sink { [weak self] editingEvent in
                self?.viewModel.userUpdatedSearch(editingEvent.textField.text)
        }
        
        viewModelCancelBag << viewModel.showRepositoryDetailsPublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] repository in
                self?.showRepositoryDetails(repository)
        }
    }
    
    private func setViewState(_ state: RepositoryListViewState) {
        let animations: () -> Void = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            // TODO: probably a more efficient way of doing this - unimportant though. Lots to do!
            switch state {
            case .displayingData:
                strongSelf.tableView.alpha = 1
                strongSelf.errorLabel.alpha = 0
                strongSelf.loadingIndicator.stopAnimating()
                strongSelf.loadingIndicator.alpha = 0
                
            case .loading:
                strongSelf.tableView.alpha = 0
                strongSelf.errorLabel.alpha = 0
                strongSelf.loadingIndicator.startAnimating()
                strongSelf.loadingIndicator.alpha = 1
                
            case let .error(errorDescription):
                strongSelf.tableView.alpha = 0
                strongSelf.errorLabel.alpha = 1
                strongSelf.errorLabel.text = errorDescription
                strongSelf.loadingIndicator.stopAnimating()
                strongSelf.loadingIndicator.alpha = 0
                
            case let .emptyData(displayString):
                strongSelf.tableView.alpha = 0
                strongSelf.errorLabel.alpha = 1
                strongSelf.errorLabel.text = displayString
                strongSelf.loadingIndicator.stopAnimating()
                strongSelf.loadingIndicator.alpha = 0
            }
            
            strongSelf.view.setNeedsLayout()
            strongSelf.view.layoutIfNeeded()
        }
        
        UIView.transition(with: view,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: animations,
                          completion: nil)
    }
    
    private func updateWithSectionViewModels(_ sectionViewModels: [RepositoryListSectionInput]) {
        sectionViewModelCancelBag.cancel()
        
        tableView.reloadData()
        hasUserScrolledAfterTableReload = false
        
        // bind to each section view model
        sectionViewModels.enumerated().forEach { (index, sectionViewModel) in
            sectionViewModelCancelBag << sectionViewModel.displayedRepositoryViewModels
                .receive(on: DispatchQueue.main)
                .sink { [weak self] arrayOperation in
                    self?.tableView.perform(operation: arrayOperation, section: index)
            }
        }
    }
    
    private func showRepositoryDetails(_ repository: Repository) {
        // close keyboard before navigation away
        view.endEditing(true)
        
        // show details
        let detailViewController = RepositoryDetailViewController.instantiate()
        let viewModel = RepositoryDetailViewModel(repository: repository)
        detailViewController.bind(to: viewModel)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension RepositoryListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionViewModels.value.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionViewModel = viewModel.sectionViewModels.value[section]
        return sectionViewModel.displayedRepositoryViewModels.elementCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionViewModel = viewModel.sectionViewModels.value[indexPath.section]
        let rowViewModel = sectionViewModel.displayedRepositoryViewModels[indexPath.row]
        
        let cell =
            tableView.dequeueCellForReusableClass(reusableClass: RepositoryItemTableViewCell.self,
                                                  indexPath: indexPath)
        
        cell.bind(to: rowViewModel)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionViewModel = viewModel.sectionViewModels.value[section]
        
        guard sectionViewModel.shouldDisplayHeader else {
            return nil
        }
        
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: RepositoryListOwnerHeaderView.reuseIdentifier) as? RepositoryListOwnerHeaderView else {
            return nil
        }
        
        headerView.bind(to: sectionViewModel.repositoryOwnerViewModel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard hasUserScrolledAfterTableReload == false else {
            // if the user has already scrolled, don't animate the cells in. This is my way of animating cells in only on initial load. 
            return
        }
        
        let delayOffset: CGFloat = 0.05
        var delay: CGFloat = 0
        if indexPath.section > 0 {
            for section in 0 ..< indexPath.section {
                delay += delayOffset * CGFloat(tableView.numberOfRows(inSection: section))
            }
        }
        delay += CGFloat(indexPath.row) * delayOffset
        delay = min(delay, 1)
        
        cell.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.5,
                       delay: TimeInterval(delay),
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0.1,
                       options: .curveLinear,
                       animations: {
                        cell.transform = .identity
        },
                       completion: nil)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hasUserScrolledAfterTableReload = true
        
        guard let visibleIndexPaths = tableView.indexPathsForVisibleRows else {
            return
        }
        
        var visibleSections = Set<Int>()
        visibleIndexPaths.forEach { visibleSections.insert($0.section) }
        
        // if the section header is at the top, add the blur. If not, remove the blur.
        visibleSections
            .compactMap { sectionIndex in
                tableView.headerView(forSection: sectionIndex) as? RepositoryListOwnerHeaderView
            }
            .forEach { headerView in
                let isHeaderAtTop = headerView.frame.minY <= 0
                headerView.setBlurHidden(isHeaderAtTop == false)
            }
    }
}

extension RepositoryListViewController: StoryboardInstantiable {
    static var storyboard: UIStoryboard {
        return .app(.repositoryList)
    }
}
