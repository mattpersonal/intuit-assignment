# Intuit Assignment 
Author: Matthew Richardson

Interview Date: November 6, 2019

## Images and Video
* [Download Video](https://bitbucket.org/mattpersonal/intuit-assignment/raw/c63b1b94ae861e619928914ba2c908132503a452/Media/sample-video.mov)
* [View Screenshots](https://bitbucket.org/mattpersonal/intuit-assignment/src/master/Media/)

## Running the Sample App
> Supported by devices running iOS 13.1 or later. 


> Requires Xcode

1. Clone the repository: `git clone https://mrrichardson52@bitbucket.org/mattpersonal/intuit-assignment.git`
2. Open the **intuit-assignment** directory: ` cd intuit-assignment`.
3. Install the required cocoapods: `pod install`. This will install two of my own frameworks: [ReactiveExtras](https://bitbucket.org/mattpersonal/reactiveextras/src/master/) and [RealTalk](https://bitbucket.org/mattpersonal/realtalk/src/master/). 
4. Open **Intuit-Assignment.xcworkspace** in **Xcode**. 
5. You should be able to run on Simulator or Device. You may need to play around with code signing and trusting profiles on your device. 

## Features
* View list of Github repositories.
* View details for a Gibhub repository. 
* View list of issues in Github repository. 
* VIew details about an issue in a Github repository. 

## Architecture
Model - View - ViewModel (MVVM) was used throughout the app. Generally, each ViewModel conforms to two protocols: <view name\>Input and <view name\>Output. 

The Input protocol specifies the information that the view needs to display the information. 

```
protocol ExampleViewInput {

    /// the screen title
    var screenTitle: String? { get }

    /// the items that need to display in a list
    var names: [String] { get }
}
```

The Output protocol specifies the functions that the view can call when the user interacts with the view. 

```
protocol ExampleViewOutput {
    func loadData()
    func userTappedItem()
}
```

The View can then use the Input and Output protocols to abstractly interact with the ViewModel that implements them. 

#### Github Repository List Page:
* Model: `[Repository]`
* View: `RepositoryListViewController`
* ViewModel: `RepositoryListViewModel`

#### Github Repository Detail Page
* Model: `Repository`
* View: `RepositoryDetailViewController`
* ViewModel: `RepositoryDetailViewModel`

#### Github Issue List Page
* Model: `[Repository.Issue]`
* View: `IssueListViewController`
* ViewModel: `IssueListViewModel`

#### Github Issue Detail Page
* Model: `Repository.Issue`
* View: `IssueDetailViewController`
* ViewModel: `IssueDetailViewModel`

## Unit Testing and UI Testing

### Code Coverage
* Unit testing code coverage: 33.7%
* UI testing code coverage: 83.6%
* Combined code coverage: 85.4%

### Testing Summary
* Unit tested `RepositoryListViewModel` to demonstrate the testing pattern for my `ViewModel` layer. 
* Unit tested the `RepositoriesService` and the `IssuesService` to demonstrate the testing pattern for my `Service` layer. 
* Wrote UI tests for the app navigation and seaching capabilities on the Repositories List page. 