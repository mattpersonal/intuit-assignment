//
//  Intuit_AssignmentUITests.swift
//  Intuit-AssignmentUITests
//
//  Created by Matthew Richardson on 11/1/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
@testable import Intuit_Assignment

class Intuit_AssignmentUITests: XCTestCase {
    
    /// Shared application
    private var app: XCUIApplication!

    override func setUp() {
        continueAfterFailure = false
        
        // Add the launch argument so that we mock data for the UI test
        app = XCUIApplication()
        app.launchArguments.append("Intuit-Assignment.Settings.mockNetworkCallsLaunchArgument")
        app.launch()
    }

    func test_search() {
        repositoryPage_searchAndSelectMockedRepo()
    }
    
    func test_navigationToDetailsPage() {
        repositoryPage_searchAndSelectMockedRepo()
        repositoryDetailPage_navigateToIssueList()
        issueListPage_navigateToIssueDetail()
        issueDetailPage_scrollToComments()
    }
}

// MARK: - Helpers

extension Intuit_AssignmentUITests {
    
    func repositoryPage_searchAndSelectMockedRepo() {
        // get the main table view on repo list page
        let tableView = app.tables["RepositoryListViewController.tableView"]
        XCTAssertTrue(tableView.waitForExistence(timeout: 3))
        
        // wait for the repository items to show
        let repoCell = app.cells["RepositoryItemTableViewCell"]
        XCTAssertTrue(repoCell.waitForExistence(timeout: 3))
        
        // enter a search string
        let searchField = app.textFields["RepositoryListViewController.searchTextField"]
        XCTAssertTrue(searchField.waitForExistence(timeout: 3))
        searchField.tap()
        searchField.typeText("Mocked-Repo")
        app.wait(seconds: 1) // not necessary - this just makes the test nicer to watch...
        
        // grab the mocked repo label and tap it
        let mockedLabel = app.staticTexts["Mocked-Repo"]
        XCTAssertTrue(mockedLabel.waitForExistence(timeout: 3))
        mockedLabel.tap()
    }
    
    func repositoryDetailPage_navigateToIssueList() {
        // tap the issues label
        let issuesLabel = app.staticTexts["Issues"]
        XCTAssertTrue(issuesLabel.waitForExistence(timeout: 3))
        issuesLabel.tap()
    }
    
    func issueListPage_navigateToIssueDetail() {
        // get the main table view on issue list page
        let tableView = app.tables["IssueListViewController.tableView"]
        XCTAssertTrue(tableView.waitForExistence(timeout: 3))
        
        // wait for the mocked issue to show and tap it
        let mockedIssueLabel = app.staticTexts["Mocked-Issue"]
        XCTAssertTrue(mockedIssueLabel.waitForExistence(timeout: 5))
        mockedIssueLabel.tap()
    }
    
    func issueDetailPage_scrollToComments() {
        var swipes = 0
        let commentCell = app.cells["IssueDetailCommentTableViewCell"]
        while swipes < 5 {
            app.swipeUp()
            
            if commentCell.exists {
                app.wait(seconds: 1) // not necessary - I just like the delay when I watch the test.
                break
            }
            
            swipes += 1
        }
    }
}
