//
//  XCUIApplication+Util.swift
//  Intuit-AssignmentUITests
//
//  Created by Matthew Richardson on 11/4/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest

extension XCUIApplication {
    
    /// Helper that waits for a few seconds just to delay.
    /// - Parameter seconds: seconds to delay
    func wait(seconds: TimeInterval) {
        let nonExistentElement = tables["There's no way that a table would have this as an accessibility identifier or label."]
        _ = nonExistentElement.waitForExistence(timeout: seconds)
    }
}
